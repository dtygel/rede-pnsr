<?php
if (isset($_GET['info'])) {
	echo phpinfo();
	exit;
}
mb_internal_encoding("UTF-8");
require_once "config.php";
require_once "funcoes_comuns.php";
//pR($options);exit;

$vinculo = ($_REQUEST['vinculo'])
	? $_REQUEST['vinculo']
	: $config->default_bond;

if ($_REQUEST['f']=='detalhes') {
	echo mostra_detalhes($_REQUEST['id'], $vinculo);
	die(0);
}

$f = (isset($_REQUEST['f'])) ? 
	$_REQUEST['f'] : 
	'mostra_rede';

if ($f=='gera_csv') {
	gera_csv_nodes_e_edges($vinculo);
}

$incorporado = isset($_REQUEST['incorporado']);
if (!$incorporado)
	$url = "";
$socorro=false;

$estilo = ($incorporado)
	? 'estilo_incorporado'
	: 'estilo_autonomo';
	
?>
<!DOCTYPE html>
<html lang="pt">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Atores do Saneamento Rural</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
		<!--<link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet"> -->
		<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet"> 
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
		
		
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
		<?php if ($f=='mostra_rede') {
				include($config->cabecalho_mostra_rede);
			} elseif ($f=='edita_tabela') {
				include('data-edit.php');
			}
		?>
	</head>
	<body>
<!-- begin template -->
<div class="navbar navbar-custom navbar-fixed-top">
 <div class="navbar-header"><a class="navbar-brand" href="#">PNSR</a>
      <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </div>
    <div class="navbar-collapse collapse">
    	<form id="form_filtros" method="GET">
		    <ul class="nav navbar-nav">
		    	<?php foreach ($rede['filters'] as $filter_slug=>$filter) { ?>
				    <li class="dropdown">
				    	<a class="dropdown-toggle" href="" data-toggle="dropdown"><?= $filter['title'] ?> <b class="caret"></b></a>
				    	<ul class="dropdown-menu filter_ul" id="<?= $filter_slug ?>">
				    		<?php foreach ($filter['data'] as $item_id=>$item) { ?>
				    			<?php $checkedtxt = (is_array($_REQUEST["c_$filter_slug"]) && in_array($item_id, $_REQUEST["c_$filter_slug"])) ? ' checked="checked"' : ""; ?>
				    			<?php /*$checkedtxt = ' checked="checked"'; */ ?>
									<li><a href="#" class="filter" id="<?=$filter_slug?>_<?=$item_id?>"><input type="checkbox" name="c_<?=$filter_slug?>" value="<?= $item_id ?>"<?= $checkedtxt ?>> <?= $item ?></a></li>
								<?php } ?>
							</ul>
				    </li>
				  <?php } ?>
		      <?php if ($f!='gera_csv') { ?>
		      	<li><a href="?f=gera_csv">Atualizar dados do drive</a></li>
		      	<li><a id="manage_custom_structures" href="#">Retratos</a></li>
		      <?php } ?>
		      <li>&nbsp;</li>
		    </ul>
		  </form>
		  <!--
      <form class="navbar-form">
        <div class="form-group" style="display:inline;">
          <div class="input-group">
            <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-chevron-down"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Category 1</a></li>
                <li><a href="#">Category 2</a></li>
                <li><a href="#">Category 3</a></li>
                <li><a href="#">Category 4</a></li>
                <li><a href="#">Category 5</a></li> 
              </ul>
            </div>
            <input type="text" class="form-control" placeholder="Buscar">
            <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span> </span>
          </div>
        </div>
      </form>
      -->
    </div>
</div>
<?php if ($f=='mostra_rede') { ?>
	<div id="map-canvas"></div>
	<div id="info"></div>
	<div id="structure-name">Nome do retrato: <span><?= $nome_estrutura ?></span></div>
	<button id="reset" class="btn btn-default" data-toggle="tooltip" title="Ver tudo"><i class="glyphicon glyphicon-resize-full"></i></button>
	<div id="nodeTooltip"></div>
<?php } ?>
<div class="container-fluid" id="main">
  <div class="row">
  	<!--
  	<div class="col-xs-0" id="left">
    
      <h2>Atores de Saneamento Rural no Brasil</h2>
      
      <!-- item list -->
      <!--
      <div class="panel panel-default">
        <div class="panel-heading"><a href="">Governo federal</a></div>
      </div>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
        Quisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis 
        dolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. 
        Aliquam in felis sit amet augue.</p>
      
      <hr>
      
      
      <!-- /item list -->
      
      <!--
      <p>
      <a href="#">Sobre</a> | <a href="#">Código fonte</a>
      </p>
      
      <hr> 
        
      <p>
      <a href="#" target="_ext" class="center-block btn btn-primary">Um botão para um <i>'saiba mais'</i>?</a>
      </p>
        
      <hr>      

    </div>
    -->
    <?php if ($f=='mostra_rede') { ?>
    	<div id="cytoscapeweb" class="col-xs-12"><!--grafo here--></div>
    <?php } elseif ($f=='edita_tabela') { ?>
    	<div id="editableTable" class="col-xs-12"></div>
    <?php } elseif ($f=='gera_csv') { ?>
    	<div class="jumbotron">
	    	<h1>A planilha foi atualizada</h1>
				<p><a class="btn btn-success" href="<?= $config->dir['url'] ?>" role="button">Ver grafo!</a></p>
			</div>
    <?php } ?>
    
  </div>
</div>
<!-- end template -->

	<!-- script references -->
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>
