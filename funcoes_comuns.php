<?php

if (!isset($options)) {
	$options = extrai_opcoes_dos_fields();
}

/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
//                                                 //
//           FUNÇÕES DO PROGRAMA!                  //
//                                                 //
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

function extrai_opcoes_dos_fields() {
	global $config;
	if (!isset($config->fields)) {
		return false;
	}
	$options = new StdClass();
	$options->fields = array();
	$options->icons = new StdClass();
	$options->filters = array();
	$options->parent_fields = array();
	$options->multiple_fields = array();
	$options->edge_fields = array();
	$options->size_field = "";
	$options->node_field = "";
	
	foreach ($config->fields as $opts) {
		$tmp = new StdClass();
		$tmp->title = $opts["label"];
		$tmp->slug = to_title($opts["label"]);
		$tmp->type = (isset($opts["type"]) && $opts["type"]) ? $opts["type"] : "string";
		if ($tmp->type == "label") {
			$options->node_field = $tmp->slug;
		}
		if ($tmp->type == "shortname") {
			$options->shortname_field = $tmp->slug;
		}
		if (isset($opts["multiple"]) && $opts["multiple"]=="true") {
			$tmp->multiple = true;
			$options->multiple_fields[$tmp->slug] = $tmp->title;
		}
		if (isset($opts["parent"]) && $opts["parent"]=="true") {
			$tmp->parent = true;
			$options->parent_fields[] = $tmp->slug;
		}
		if (isset($opts["filter"])) {
			$tmp->filter = true;
			$options->filters[$tmp->slug] = (isset($opts["menu_label"]))
				? $opts["menu_label"]
				: $tmp->title;
		}
		if (isset($opts["edge"])) {
			$options->edge_fields[] = $tmp->slug;
		}
		if (isset($opts["size"])) {
			$options->size_field = $tmp->slug;
		}
		if (isset($opts["size_intervals"])) {
			$options->size_intervals = $opts["size_intervals"];
		}
		if (isset($opts["icons"])) {
			$options->icons->field = $tmp->slug;
			$options->icons->icons = array_keys_to_title($opts["icons"]);
		}
		$options->fields[$tmp->slug] = $tmp;
	}
	return $options;
}

function gera_csv_nodes_e_edges($vinculo, $fonte='googleDrive') {
	global $config, $options;
	if (!$vinculo) {
		$vinculo = $config->default_bond;
	}
	if ($fonte=='googleDrive') {
		require 'get_data_from_google_drive.php';
		$dados = get_data_from_google_drive();
		if (!$dados) {
			// Something went wrong authenticating in google
			return false;
		}
		
		$tits = array_shift($dados);
		$dadosJson = array();
		foreach ($dados as $d) {
			$tmp = new StdClass();
			foreach ($tits as $i=>$tit) {
				$tmp->{to_title($tit)} = $d[$i];
			}
			$dadosJson[] = $tmp;
		}
		file_put_contents($config->arq_cache, json_encode($dadosJson));
	} else {
		$dados = array();
		if (($handle = fopen($config->arq_db, "r")) !== FALSE) {
			$tits = fgetcsv($handle, 10000, "\t");
			$i=1;
			while (($d = fgetcsv($handle, 10000, "\t")) !== FALSE) {
				$dados[$i] = $d;
				$i++;
			}
		}
		fclose($handle);
	}
	
	$rede = array();
	$rede['nodes'] = array();
	$rede['edges'] = array();
	$rede['filters'] = array();
	foreach ($options->filters as $filter_field=>$filter_title) {
		if (!isset($rede['filters'][$filter_field])) {
			$rede['filters'][$filter_field] = array();
			$rede['filters'][$filter_field]['slug'] = $filter_field;
			$rede['filters'][$filter_field]['title'] = $filter_title;
			$rede['filters'][$filter_field]['data'] = array();
		}
	}
	foreach ($dados as $i=>$d) {
		$r = array();
		foreach ($tits as $j=>$t) {
			$r[to_title($t)] = trim($d[$j]);
		}
		$id_ator = false;
		foreach ($rede['nodes'] as $n) {
			if ($n['slug']==to_title($r[$options->node_field])) {
				$id_ator = $n['id'];
			}
		}
		if (!$id_ator) {
			$id_ator = $r['id'];
			$novo = array();
			$novo['id'] = $id_ator;
			$novo['label'] = ($r[$options->shortname_field])
				? $r[$options->shortname_field]
				: $r[$options->node_field];
			$novo['fullname'] = $r[$options->node_field];
			$novo['slug'] = to_title($r[$options->node_field]);
			if (isset($options->size_field)) {
				if (!isset($options->size_intervals)) {
					$novo['size'] = floatval($r[$options->size_field]);
				} else {
					$ref_size = floatval($r[$options->size_field]);
					$min = 0;
					foreach ($options->size_intervals as $max=>$size) {
						if ($ref_size>=$min && $ref_size<$max) {
							$novo['size'] = $size;
							break;
						}
						$min = $max;
					}
				}
			} else {
				$novo['size'] = floatval($config->default_size);
			}
			
			$novo['icon'] = to_title($r[$options->icons->field]);
			$novo['parents'] = array();
			foreach ($options->parent_fields as $parent_field) {
				if (isset($options->multiple_fields[$parent_field])) {
					$parents = explode(',',$r[$parent_field]);
					foreach ($parents as $parent) {
						$p = trim($parent);
						if ($p) {
							$novo['parents'][] = array('label'=>$p, 'slug'=>to_title($p), 'id'=>null, 'parent_field'=>$parent_field);
						}
					}
				} else {
					$p = trim($r[$parent_field]);
					if ($p) {
						$novo['parents'][] = array('label'=>$p, 'slug'=>to_title($p), 'id'=>null, 'parent_field'=>$parent_field);
					}
				}
			}
			// Define os filtros:
			foreach ($options->filters as $filter_field=>$filter_title) {
				if ($r[$filter_field]) {
					$item_filters = ($options->multiple_fields[$filter_field])
						? explode(',',$r[$filter_field])
						: array($r[$filter_field]);
					$filterIds = array();
					foreach ($item_filters as $item_filter) {
						$item_filter_slug = to_title($item_filter);
						$item_filter_title = trim($item_filter);
						if (!isset($rede['filters'][$filter_field]['data'][$item_filter_slug])) {
							$rede['filters'][$filter_field]['data'][$item_filter_slug] = $item_filter_title;
						}
						$filterIds[] = array_search($item_filter_slug, array_keys($rede['filters'][$filter_field]['data']));
					}
					$novo['filter-'.$filter_field] = implode('|',$filterIds);
				} else {
					$novo['filter-'.$filter_field] = '-1';
				}
			}
			
			$rede['nodes'][$r['id']] = $novo;
		}			
	} // fim do loop na tabela-fonte
	
	// Complementar lista de nós com parents que não tenham sido incluídos na própria lista
	$nodesExpanded = $rede['nodes'];
	$addedNodes = array();
	$addedNodesIds = array();
	foreach ($rede['nodes'] as $id=>$n) {
		$parents = $n['parents'];
		foreach ($parents as $parent_pos=>$parent) {
			if (!in_array($parent['slug'], $addedNodes)) {
				$exists = false;
				foreach ($rede['nodes'] as $p) {
					if ($parent['slug']==$p['slug'] || $parent['slug']==to_title($p['label'])) {
						$nodesExpanded[$id]['parents'][$parent_pos]['id'] = $p['id'];
						if ($parent_pos==0) {
							//$nodesExpanded[$id]['parent'] = $p['id'];
						}
						$exists = true;
						break;
					}
				}
				if (!$exists) {
					$lastNode = array_pop((array_slice($nodesExpanded, -1)));
					$novo = array();
					$novo['id'] = 'auto_'.(count($addedNodes)+1);
					$novo['label'] = $parent['label'];
					$novo['fullname'] = $parent['label'];
					$novo['slug'] = $parent['slug'];
					$novo['size'] = floatval($config->default_parent_size);
					$novo['icon'] = to_title($config->default_parent_icon);
					$novo['parents'] = array();
					
					
					foreach ($options->filters as $filter_field=>$filter_title) {
						switch($filter_field) {
							case $options->node_field:
								$field = 'label';
							break;
							case $options->shortname_field:
								$field = 'shortname';
							break;
							default:
								$field = $filter_field;
							break;
						}
						if ($novo[$field]) {
							$item_filters = ($options->multiple_fields[$filter_field])
								? explode(',',$novo[$field])
								: array($novo[$field]);
							$filterIds = array();
							foreach ($item_filters as $item_filter) {
								$item_filter_slug = to_title($item_filter);
								$item_filter_title = trim($item_filter);
								if (!isset($rede['filters'][$filter_field]['data'][$item_filter_slug])) {
									$rede['filters'][$filter_field]['data'][$item_filter_slug] = $item_filter_title;
								}
								$filterIds[] = array_search($item_filter_slug, array_keys($rede['filters'][$filter_field]['data']));
							}
							$novo['filter-'.$filter_field] = implode('|',$filterIds);
						} else {
							$novo['filter-'.$filter_field] = '-1';
						}
					}
					
					$nodesExpanded[] = $novo;
					$addedNodes[] = $novo['slug'];
					$addedNodesIds[] = $novo['id'];
					
					$nodesExpanded[$id]['parents'][$parent_pos]['id'] = $novo['id'];
					if ($parent_pos==0) {
						//$nodesExpanded[$id]['parent'] = $novo['id'];
					}
				}

			} else {
				foreach ($addedNodes as $addedNodePos=>$addedNode) {
					if ($parent['slug'] == $addedNode) {
						$nodesExpanded[$id]['parents'][$parent_pos]['id'] = $addedNodesIds[$addedNodePos];
						break;
					}
				}
			}
		}
	}
	$rede['nodes'] = $nodesExpanded;
	
	// Edges:
	foreach ($rede['nodes'] as $node) {
		$id = $node['id'];
		foreach ($node['parents'] as $parent) {
			$novo = array();
			$novo['id'] = 'e'.(count($rede['edges'])+1);
			$novo['source'] = $id;
			$novo['target'] = $parent['id'];
			$novo['interaction'] = $parent['parent_field'];
			$rede['edges'][] = $novo;
		}
	}
	
	// Salvar edges:
	$arq = fopen("dados/edges_$vinculo.csv", 'w');
	fputcsv($arq, array_keys($rede['edges'][0]));
	foreach ($rede['edges'] as $r)
		fputcsv($arq, $r);
	fclose($arq);
	
	// Salvar nodes:
	$arq = fopen("dados/nodes_$vinculo.csv", 'w');
	fputcsv($arq, array_keys(array_values($rede['nodes'])[0]));
	foreach ($rede['nodes'] as $r) {
		$parentsIds = array();
		foreach ($r['parents'] as $p) {
			$parentsIds[] = $p['id'];
		}
		$to_save = $r;
		$to_save['parents'] = implode('|',$parentsIds);
		fputcsv($arq, $to_save);
	}
	fclose($arq);
	
	// Salvar filters:
	$arq = fopen("dados/filters.csv", 'w');
	fputcsv($arq, array_keys(get_object_vars(array_values($rede['filters'])[0])));
	foreach ($rede['filters'] as $filter_slug=>$filter) {
		$to_save = $filter;
		$to_save['data'] = implode('|', $filter['data']);
		fputcsv($arq, $to_save);
	}
	fclose($arq);
}

function le_estrutura($estrutura) {
	$dadosEstruturaRaw = file_get_contents("dados/estruturas/$estrutura.json");
	$dadosEstruturaRaw = json_decode($dadosEstruturaRaw);
	$dadosEstrutura = new StdClass();
	$dadosEstrutura->name = $dadosEstruturaRaw->name;
	$dadosEstrutura->data = array();
	foreach ($dadosEstruturaRaw->data as $d) {
		$dadosEstrutura->data[$d->id] = $d->position;
	}
	return $dadosEstrutura;
}

function le_csv_nodes_e_edges($opcoes=array(), $estrutura) {
	global $config, $options;
	
	$arq = ($opcoes['vinculo'])
		? array('nodes'=>'dados/nodes_'.$opcoes['vinculo'].'.csv', 'edges'=>'dados/edges_'.$opcoes['vinculo'].'.csv')
		: array('nodes'=>'dados/nodes_'.$config->default_bond.'.csv', 'edges'=>'dados/edges_'.$config->default_bond.'.csv');
	$arq['filters'] = 'dados/filters.csv';
	$rede = array();
	
	$dadosEstrutura = le_estrutura($estrutura);
	
	// Leitura dos nodes:
	if ($config->cytoscapejs) {
		$rede['elements'] = array();
	} else {
		$rede['nodes'] = array();
	}
	
	if (($handle = fopen($arq['nodes'], "r")) !== FALSE) {
		$tits = fgetcsv($handle, 10000, ",");
		while (($ds = fgetcsv($handle, 10000, ",")) !== FALSE) {
			$node = array();
			foreach ($ds as $c=>$d) {
				$tit_slug = to_title($tits[$c]);
				$opt = $option->fields[$tit_slug];
				if ($opt->multiple) {
					if ($opt->type=='integer' || $opt->parent || $tit_slug=='size') {
						$tmp = explode('|', $d);
						$node[$tit_slug] = array();
						foreach ($tmp as $t) {
							$node[$tit_slug][] = floatval($t);
						}
					} else {
						$node[$tit_slug] = explode('|', $d);
					}
				} else {
					switch ($opt->type) {
						case 'date':
							$node[$tit_slug] = $d;
						break;
						case 'integer':
							$node[$tit_slug] = floatval($d);
						break;
						default:
							$node[$tit_slug] = ($tit_slug=='size')
								? floatval($d)
								: $d;
						break;
					}
				}
			}
			$node['position'] = $dadosEstrutura->data[$node['id']];
			if ($config->cytoscapejs) {
				$tmp = new StdClass();
				$tmp->data = $node;
				$rede['elements'][] = $tmp;
			} else {
				$rede['nodes'][] = $node;
			}
			
		}
	} else return false;
	fclose($handle);
	
	// Leitura dos edges:
	if (!$config->cytoscapejs) {
		$rede['edges'] = array();
	}
	
	if (($handle = fopen($arq['edges'], "r")) !== FALSE) {
		$tits = fgetcsv($handle, 10000, ",");
		while (($ds = fgetcsv($handle, 10000, ",")) !== FALSE) {
			$edge = array();
			foreach ($ds as $c=>$d) {
				$edge[to_title($tits[$c])] = $d;
			}
			if ($config->cytoscapejs) {
				$tmp = new StdClass();
				$tmp->data = $edge;
				$rede['elements'][] = $tmp;
			} else {
				$rede['edges'][] = $edge;
			}
			
		}
	} else return false;
	
	// Leitura dos filters:
	if (($handle = fopen($arq['filters'], "r")) !== FALSE) {
		$tits = fgetcsv($handle, 10000, ",");
		$rede['filters'] = array();
		while (($ds = fgetcsv($handle, 10000, ",")) !== FALSE) {
			$filter_slug = $ds[0];
			$rede['filters'][$filter_slug] = array();
			$rede['filters'][$filter_slug]['title'] = $ds[1];
			$rede['filters'][$filter_slug]['data'] = explode('|', $ds[2]);
		}
	} else return false;
	fclose($handle);
	return $rede;
}

function mostra_detalhes($id, $opcoes=array()) {
	$html = array();
	$html['atores'] = $html['links'] = $html['produtos'] = array();
	$rede = le_csv_nodes_e_edges($opcoes);
	$id_atores = array();
	$produtos_relacionados = array();
	$id_links = array();
	foreach ($rede['edges'] as $r) {
		if ($r['source']==$id) {
			$target = node($r['target'], $opcoes);
			$extras = json_decode($target['extra']);
			foreach ($rede['edges'] as $s) {
				if ($s['target'] == $r['target']) {
					// Atores:
					if ($s['source']!=$id && !in_array($s['source'], $id_atores)) {
						$source = node($s['source']);
						$id_atores[] = $source['id'];
						$html['atores'][] = '<li> ' . $source['label'] . ' (' . $source['tipo'] . ')</li>';
					}
					// Produtos:
					$produtos = explode(',',$extras->produtos_biofortificados);
					foreach ($produtos as $p) {
						$produto = str_replace('-', ' ', $p);
						if ($produto && !in_array($produto, $produtos_relacionados)) {
							$produtos_relacionados[] = $produto;
							$html['produtos'][] = '<li> ' . $produto . '</li>';
						}
					}
				}
			}
			if ($opcoes['vinculo']=='url') {
				$id_links[] = $target['id'];
				$html['links'][] = '<li><a href="'.$extras->url.'" target="_blank" title="'.$extras->conteudo.'">' . trim_text($extras->conteudo,70) . '</a> <i>(em '.$target['label'].')</i></li>';
			}
		} elseif ($opcoes['vinculo']=='produto' && $r['target']==$id) {
			$source = node($r['source'], $opcoes);
			$extras = json_decode($source['extra']);
			// Atores:
			$id_atores[] = $source['id'];
			$html['atores'][] = '<li> ' . $source['label'] . ' (' . $source['tipo'] . ')</li>';
		}
	}
	asort($html['atores']);
	asort($html['produtos']);
	$html['atores'] = implode('',$html['atores']);
	$html['links'] = implode('',$html['links']);
	$html['produtos'] = implode('',$html['produtos']);
	return json_encode($html);
}

function node($id, $opcoes=array()) {
	$rede = le_csv_nodes_e_edges($opcoes);
	foreach ($rede['nodes'] as $r)
		if ($r['id']==$id)
			return $r;
	return false;
}

function resgata_categorias($tipo_cat, $opcoes=array()) {
	/*$arq = ($opcoes['hide_url_nodes'])
		? array('nodes'=>'dados/nodes_sem_urls.csv', 'edges'=>'dados/edges_sem_urls.csv')
		: array('nodes'=>'dados/nodes.csv', 'edges'=>'dados/edges.csv');*/
	/*$arq = ($opcoes['vinculo'])
		? array('nodes'=>'dados/nodes_'.$opcoes['vinculo'].'.csv', 'edges'=>'dados/edges_'.$opcoes['vinculo'].'.csv')
		: array('nodes'=>'dados/nodes_url.csv', 'edges'=>'dados/edges_url.csv');*/
	$arq = array('nodes'=>'dados/nodes_url.csv', 'edges'=>'dados/edges_url.csv');
	switch ($tipo_cat) {
		case 'tipos_atores':
		case 'sites_base':
			if (($handle = fopen($arq['nodes'], "r")) !== FALSE) {
				$tits = fgetcsv($handle, 10000, ",");
				$key_tipo = array_search('tipo', $tits);
				$key_label = array_search('label', $tits);
				$tipos_atores = array();
				$sites_base = array();
				while (($ds = fgetcsv($handle, 10000, ",")) !== FALSE) {
					if ($tipo_cat=='tipos_atores' && $ds[$key_tipo] && $ds[$key_tipo] != 'url' && !in_array($ds[$key_tipo], $tipos_atores)) {
						$tipos_atores[] = $ds[$key_tipo];
					} elseif ($tipo_cat=='sites_base') {
						$site_base = trim(substr($ds[$key_label],0,-6));
						if ($site_base && $ds[$key_tipo] == 'url' && !in_array($site_base, $sites_base))
							$sites_base[] = $site_base;
					}
				}
				if ($tipos_atores) {
					asort($tipos_atores);
					return $tipos_atores;
				}
				if ($sites_base) {
					asort($sites_base);
					return $sites_base;
				}
			}
		break;
		case 'produtos':
			if (($handle = fopen($arq['nodes'], "r")) !== FALSE) {
				$tits = fgetcsv($handle, 10000, ",");
				$key_tipo = array_search('tipo', $tits);
				$key_extra = array_search('dados', $tits);
				$produtos = array();
				while (($ds = fgetcsv($handle, 10000, ",")) !== FALSE) {
					$extras = explode('|', $ds[$key_extra]);
					$prods = array();
						foreach ($extras as $extra) {
							list($extra_c, $extra_v) = explode('=', $extra);
							if ($extra_c=='produtos_biofortificados') {
								$prods = explode(',',$extra_v);
								break;
							}
						}
						if ($prods) {
							foreach ($prods as $p) {
								$t = str_replace('-',' ',$p);
								if ($p && !in_array($t, $produtos))
								$produtos[] = $t;
							}
						}
				}
				if ($produtos) {
					asort($produtos);
					return $produtos;
				}
			}
		break;
		case 'paises':
		break;
		
	}
}

function to_title($str, $toLowerCase = true, $exclude = array())
{
	// replace accents by equivalent non-accents
	//$str = replaceAccents($str);
	$str = tira_acentos(trim($str));
	// space before a captial letter
	$capregex = '/(\B[A-Z])(?=[a-z])|(?<=[a-z])([A-Z])/sm';
	$replace = ' $1$2';
	$str = preg_replace($capregex, $replace, $str);
	// to lower case
	if($toLowerCase) $str = strtolower($str);
	// non-alpha and non-numeric characters become dashes
	return trim(preg_replace('/[^a-z0-9' . implode("", $exclude) . ']+/i', '-', $str),'-');
}
function replaceAccents($str) {
	$search = explode(",",
"ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ");
	$replace = explode(",",
"c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE");
	return str_replace($search, $replace, $str);
}

function array_keys_to_title($arr) {
	if (!is_array($arr)) {
		return false;
	}
	$ret = array();
	foreach ($arr as $key=>$value) {
		$ret[to_title($key)] = $value;
	}
	return $ret;
}



//*
//*
//*
//* Formata números com o separador de milhar (".") e com o separador de decimais (",").
//*
//*
//*
function yLabelFormat($aLabel) {
		return number_format($aLabel, 0, ',', '.');
} 




//*
//*
//*
//* Tira os slashes de matrizes
//*
//*
//*
function stripslashes_array($resultado)
{
	if (is_array($resultado)) $resultado=array_map('stripslashes_array', $resultado);
		else $resultado=stripslashes($resultado);
	return $resultado;
}

//*
//*
//*
//* Faz o query e escreve o erro, caso não tenha dado certo!
//*
//*
//*
function faz_query($sql,$con='',$tipo='',$assoc='', $encode=false) {
	$keys=array();
	$res=mysql_query($sql);
	if (!$res) {
		echo '<p>Deu pau no mysql! O comando sql que foi tentado foi o seguinte:</p>'.chr(10);
		echo '<p>'.$sql.'</p>'.chr(10);
		echo '<p>E o erro anunciado foi o seguinte: "'.mysql_error().'"</p>'.chr(10);	
		exit;
	}	
	switch ($tipo) {
		default:
			return $res;
		break;
		case 'array':
			$i=0;
			while ($q=mysql_fetch_array($res, MYSQL_ASSOC)) {
				if (!$keys) $keys=array_keys($q);
				foreach ($keys as $k)
					$retorno[$k][$i]=$q[$k];
				$i++;
			}			
			/*if ($encode)
				$retorno = converte_charset2($retorno);*/
		break;
		case 'array_assoc':
			$i=0;
			while ($q=mysql_fetch_array($res, MYSQL_ASSOC)) {
				if (!$keys) $keys=array_keys($q);
				foreach ($keys as $k)
					if ($k<>$assoc) $retorno[$q[$assoc]][$k]=$q[$k];
				$i++;
			}
			/*if ($encode)
				$retorno = converte_charset2($retorno);*/
		break;
		case 'row':
		case 'rows':
			$i=0;
			while ($q=mysql_fetch_row($res)) {
				if (!$keys) $keys=array_keys($q);
				foreach ($keys as $k)
					$retorno[$k][$i]=$q[$k];
				$i++;
			}
			/*if ($encode)
				$retorno = converte_charset2($retorno);*/
		break;
		case 'object':
			$i=0;
			while ($q=mysql_fetch_object($res)) {				
				$arr=get_object_vars($q);
				if (!$keys) $keys=array_keys($arr);
				foreach ($keys as $k)
					$retorno[$i]->$k=$q->$k;
				$i++;
			}
			/*if ($encode)
				$retorno = converte_charset2($retorno);*/
		break;
		case 'object_assoc':
			$i=0;
			while ($q=mysql_fetch_object($res)) {
				$arr=get_object_vars($q);
				if (!$keys) $keys=array_keys($arr);
				foreach ($keys as $k) 
					$retorno[$q->$assoc]->$k=$q->$k;
				$i++;
			}
			/*if ($encode)
				$retorno = converte_charset2($retorno);*/
		break;
		case 'num':
		case 'numrows':
		case 'num_rows':
			$retorno=mysql_num_rows($res);
		break;
		case 'id':
			$retorno=mysql_insert_id();
		break;
	}
	return $retorno;
}


// Pega um texto e dá a sua resposta na língua $lingua. 
// O texto tem que estar no seguinte formato: "lng1:txtlng1|lng2:txtlng2|lng3:txtlng3" etc. 
// Se não estiver neste formato ou não houver a língua $lingua, devolve simplesmente o primeiro txt
function traduz($txt,$lingua='') {
	$tmps = explode('|',$txt);
	unset($tit);
	foreach ($tmps as $i=>$tmp) {
		$tmp2 = explode(':',$tmp);
		if (strlen($tmp2[0])>5) // pra evitar considerar dois pontos de texto normal
			$tit[$i]=$tmp;
			elseif ($tmp2[1])
				$tit[$tmp2[0]]=str_replace($tmp2[0].':','',$tmp);
			else $tit[$i]=$tmp2[0];
	}
	$keys = array_keys($tit);
	if ($lingua)
		$txt = (!$tit[$lingua])
			? ($tit[substr($lingua,0,2)])
				? $tit[substr($lingua,0,2)]
				: $tit[$keys[0]]
			: $tit[$lingua];
	else $txt=$tit[$keys[0]];
	return $txt;
}


//*
//*
//* Faz a conexão no banco de dados
//*
//*
function conecta($bd) {
	$con=@mysql_connect($bd['servidor'], $bd['usuaria'], $bd['senha']) or die ('<b>[06] Erro:</b> durante a conexão ao banco de dados.<br>O servidor MySQL disse: "<i>'.mysql_error().'</i>"');
	mysql_query("SET CHARACTER SET utf8");
	return $con;
}

// Bobagens, para eu poder fingir que coloco dados, mas sem colocar de verdade...
function faz_query2($texto, $con) {
	echo $texto.'<br>';
	return true;
}

//*
//*
//* Elimina acentos de um texto.
//*
//*
function tira_acentos($texto) {	
		$tab['sem_acentos']=array(192=>"A", 193=>"A", 194=>"A", 195=>"A", 196=>"A", 197=>"A", 198=>"A", 199=>"C", 200=>"E", 201=>"E", 202=>"E", 203=>"E", 204=>"I", 205=>"I", 206=>"I", 207=>"I", 208=>"O", 209=>"N", 210=>"O", 211=>"O", 212=>"O", 213=>"O", 214=>"O", 216=>"O", 217=>"U", 218=>"U", 219=>"U", 220=>"U", 221=>"Y", 224=>"a", 225=>"a", 226=>"a", 227=>"a", 228=>"a", 229=>"a", 230=>"a", 231=>"c", 232=>"e", 233=>"e", 234=>"e", 235=>"e", 236=>"i", 237=>"i", 238=>"i", 239=>"i", 241=>"n", 242=>"o", 243=>"o", 244=>"o", 245=>"o", 246=>"o", 248=>"o", 249=>"u", 250=>"u", 251=>"u", 252=>"u", 253=>"y", 255=>"y");
		$tab['com_acentos']=array(chr(192), chr(193), chr(194), chr(195), chr(196), chr(197), chr(198), chr(199), chr(200), chr(201), chr(202), chr(203), chr(204), chr(205), chr(206), chr(207), chr(208), chr(209), chr(210), chr(211), chr(212), chr(213), chr(214), chr(216), chr(217), chr(218), chr(219), chr(220), chr(221), chr(224), chr(225), chr(226), chr(227), chr(228), chr(229), chr(230), chr(231), chr(232), chr(233), chr(234), chr(235), chr(236), chr(237), chr(238), chr(239), chr(241), chr(242), chr(243), chr(244), chr(245), chr(246), chr(248), chr(249), chr(250), chr(251), chr(252), chr(253), chr(255));
		$texto=str_replace($tab['com_acentos'],$tab['sem_acentos'],utf8_decode($texto));
		return($texto);
}

function insere_html_comentario ($nome, $email, $data_criacao, $organizacao, $texto, $anexo, $anexo_tipo, $video) {
	global $obj_youtube, $img_youtube, $dir;
	$pre = "<p class='mensagem_balao";
	$css='_corpo';
	$css2='_assunto';
	$pos="</p>".chr(10);
	$html .= $pre."_data'>".$data_criacao.$pos;
	$html .= $pre.$css2."'>";
	$html .= "<span class='span_mensagem_balao_corpo'>De</span>: $nome";
	$html .= $pos;
	if ($organizacao)
		$html .= $pre.$css2."'><span class='span_mensagem_balao_corpo'>Organização</span>: $organizacao".$pos;
	$html .= $pre.$css."'>";
	//$html .= "<span class='span_mensagem_balao_corpo'>Depoimento</span>: <br />";
	if ($anexo)
		$html .= ($anexo_tipo=='imagem')
			? "<div style=\"float:left;margin:0 10px 5px 0;border: solid 1px #ccc\" ><a href=\"javascript:abreImagem ('".$dir['uploads_url'].$anexo."','','imagem')\" title=\"\"><img src=\"".$dir['uploads_url']."peq/$anexo\"><br>clique para ampliar</a></div>"
			: "<span class='span_mensagem_balao_corpo'>Documento anexo</span>: <a href=\"".$dir['uploads_url'].$anexo."\">$anexo</a>".$pos.$pre.$css."'>";
	$html .= $texto;
	if ($video)
		$html .= "<br><center><a href=\"javascript:abreImagem ('$video','','video')\" title=\"\"><img style=\"margin:0 5px 5px 0\" src=\"".str_replace('{cod}',$video,$img_youtube)."\"><br>clique para assistir ao vídeo</a></center>";
	$html .= $pos;
	return $html;
}

function pR($v) {
	echo "<pre>";
	print_r($v);
	echo "</pre>";
}

function formata_registro($reg) {
	if (strlen($reg)==11)
		return array(
			'tipo'=>'RG', 
			'num_formatado'=>substr($reg,0,3).'.'.substr($reg,3,3).'.'.substr($reg,6,3).'-'.substr($reg,9,2),
			'num'=>$reg
		);
		elseif (strlen($reg)==14)
			return array(
				'tipo'=>'CNPJ', 
				'num_formatado'=>substr($reg,0,2).'.'.substr($reg,2,3).'.'.substr($reg,5,3).'/'.substr($reg,8,4).'-'.substr($reg,12),
				'num'=>$reg
			);
		else
			return $reg;
}

/**
 * trims text to a space then adds ellipses if desired
 * @param string $input text to trim
 * @param int $length in characters to trim to
 * @param bool $ellipses if ellipses (...) are to be added
 * @param bool $strip_html if html tags are to be stripped
 * @return string
 */
function trim_text($input, $length, $ellipses = true, $strip_html = false) {
		//strip tags, if desired
		if ($strip_html) {
				$input = strip_tags($input);
		}
	
		//no need to trim, already shorter than trim length
		if (strlen($input) <= $length) {
				return $input;
		}
	
		//find last space within length
		$last_space = strrpos(substr($input, 0, $length), ' ');
		$trimmed_text = substr($input, 0, $last_space);
	
		//add ellipses (...)
		if ($ellipses) {
				$trimmed_text .= '...';
		}
	
		return $trimmed_text;
}

?>
