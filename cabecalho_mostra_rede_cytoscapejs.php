<?php

	$largura = (isset($_REQUEST['largura']))
		? $_REQUEST['largura']
		: '500';
	
	$dir = (isset($_REQUEST['dir']))
		? $_REQUEST['dir']
		: 'todos';
		
	if ($incorporado)
		$linkPara = "&incorporado=1";
	
	// Opções da rede a ser gerada:
	$vinculo = (isset($_REQUEST['vinculo']))
		? $_REQUEST['vinculo']
		: $config->default_bond;
	$opcoes = array(
		'vinculo'=>$vinculo
	);
	
	// Estrutura da rede:
	$estrutura_carregada = (isset($_GET['estrutura']) && $_GET['estrutura']!='')
		? $_GET['estrutura']
		: 'default';
	
	// A rede:
	$rede = le_csv_nodes_e_edges($opcoes, $estrutura_carregada);
	$estrutura = le_estrutura($estrutura_carregada);
	$positions = $estrutura->data;
	$nome_estrutura = $estrutura->name;
	$estrutura_padrao = le_estrutura('default');
	$estrutura_padrao = $estrutura_padrao->name;
	$nodes_json = "nodes: ".json_encode($rede['nodes']);
	$edges_json = "edges: ".json_encode($rede['edges']);
	
	$node_keys = array();
	foreach (array_values($rede['nodes'])[0] as $key=>$r) {
		$novo = new StdClass();
		$novo->key = $key;
		$novo->type = ($key=='size') ? "number" : "string";
		$node_keys[] = $novo;
	}
?>
<!--<script src="js/cytoscape.min.js"></script>
<script src="js/arbor.js"></script>
<script src="js/cytoscape-arbor.js"></script>-->

<script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.2/handlebars.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/cytoscape/3.2.1/cytoscape.min.js"></script>
<script src="//marvl.infotech.monash.edu/webcola/cola.v3.min.js"></script>
<script src="js/cytoscape-cola.js"></script>

<!--<script type="text/javascript" src="cytoscapeweb/js/json2.min.js"></script>
<script type="text/javascript" src="cytoscapeweb/js/AC_OETags.min.js"></script>
<script type="text/javascript" src="cytoscapeweb/js/cytoscapeweb.min.js"></script>
<script src="jquery.tools.min.js"></script>-->
<script src="funcoes.js"></script>
<script src="js/scripts_grafo.js"></script>
<script type="text/javascript">
	var layoutOptions = {
		name: 'preset',
		positions: <?= json_encode($positions) ?>,
		padding: layoutPadding
	};
	var elements = <?= json_encode($rede['elements']) ?>;
	var structureName = "<?= $nome_estrutura ?>";
	
	$(function () {
		$.ajax({
  	  url: 'css/stylesCy.css',
  	  type: 'GET',
  	  dataType: 'text'
  	}).done(function(data) {
  		initCy(data);
  	});
	});
</script>
<?php

?>
