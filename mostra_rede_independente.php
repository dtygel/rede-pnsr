<?php
	mb_internal_encoding("UTF-8");
	require_once "config.php";
	require_once "funcoes_comuns.php";
	$con=conecta($bd);
	mysql_select_db($bd['eita_proprietariosdobrasil']);
	$largura = ($_REQUEST['largura'])
		? $_REQUEST['largura']
		: '500';
	
	$id = ($_REQUEST['id'])
		? $_REQUEST['id']
		: 10;
		
	$dir = ($_REQUEST['dir'])
		? $_REQUEST['dir']
		: 'todos';
	
	$rede=array();
	if (in_array($dir,array('todos','composicao'))) {
		$p1 = busca_recursiva('composicao',array(),$id);
		foreach ($p1 as $p)
			$rede[]=$p;
	}
	if (in_array($dir,array('todos','participacao'))) {
		$p2 = busca_recursiva('participacao',array(),$id);
		foreach ($p2 as $p)
			if (!in_array($p,$rede))
				$rede[]=$p;
	}

	$redetxt = implode(',',$rede);
	//$sql = "SELECT DISTINCTROW a.id, a.id_empresaControladora, IF(a.id_empresaControladora=0 OR c.tipo='Pessoa',IF(a.id_empresasControladas='' OR a.tipo='Pessoa',0,1),0) as ehControladoraUltima, IF(a.id_empresaControladora>0,1,0) as ehControlada, a.nome, a.registro, a.tipo, a.nacMaj, b.IPA FROM assoc a LEFT JOIN IPA b ON b.id_empresa=a.id LEFT JOIN assoc c ON c.id=a.id_empresaControladora WHERE a.id IN ($redetxt)";
	$sql = "SELECT DISTINCTROW a.id, a.id_empresaControladora, a.ehControladoraUltima, a.ehParticipanteInterCadeias, IF(a.id_empresaControladora>0,1,0) as ehControlada, a.nome, a.registro, a.tipo, a.nacMaj, b.IPA FROM assoc a LEFT JOIN IPA b ON b.id_empresa=a.id LEFT JOIN assoc c ON c.id=a.id_empresaControladora WHERE a.id IN ($redetxt)";
	//echo $sql;exit;
	$res=faz_query($sql,'','object');
	$sql = "SELECT IPA FROM IPA ORDER BY IPA DESC LIMIT 1";
	$IPAmax = faz_query($sql,'','object');
	$IPAmax = $IPAmax[0]->IPA;
	$nodes = array();
	foreach ($res as $i=>$r) {
		if ($r->id==$id)
			$nome = $r->nome;
		$nodes[$i]->id=$r->id;
		$nodes[$i]->label=$r->nome;
		$nodes[$i]->ehControlada=intval($r->ehControlada);
		$nodes[$i]->ehControladoraUltima=intval($r->ehControladoraUltima);
		$nodes[$i]->ehParticipanteInterCadeias=intval($r->ehParticipanteInterCadeias);
		if ($nodes[$i]->ehControladoraUltima)
			$nodes[$i]->tipoRanking="controladoraUltima";
			elseif ($nodes[$i]->ehParticipanteInterCadeias)
				$nodes[$i]->tipoRanking="participanteInterCadeias";
				else 
					$nodes[$i]->tipoRanking="naoRankeada";
		$nodes[$i]->tipoTxt = ($r->tipo)
			? $r->tipo
			: "Empresa";
		$nodes[$i]->tipo = ($r->id==$id)
			? "Foco".$nodes[$i]->tipoTxt
			: $nodes[$i]->tipoTxt;
		$nodes[$i]->IPA=floatval($r->IPA);
		$nodes[$i]->IPAtam=floatval(500*($r->IPA/$IPAmax));
	}
	$sql = "SELECT DISTINCTROW a.*, b.nome as nomeSource, c.nome as nomeTarget FROM relacoes a, assoc b, assoc c WHERE b.id=a.id_empresa AND c.id=a.id_empresaMae AND a.id_empresa IN ($redetxt) AND a.id_empresaMae IN ($redetxt)";
	$res=faz_query($sql,'','object');
	$edges = array();
	foreach ($res as $i=>$r) {
		$edges[$i]->id=$r->id;
		$edges[$i]->source=$r->id_empresa;
		$edges[$i]->target=$r->id_empresaMae;
		$edges[$i]->weight=floatval($r->partRelBattiston);
		$edges[$i]->weightTxt=number_format($r->partRelBattiston,2,',','.').'%';
		$edges[$i]->nameSource=$r->nomeSource;
		$edges[$i]->nameTarget=$r->nomeTarget;
	}
	$nodes = "nodes: ".json_encode($nodes);
	$edges = "edges: ".json_encode($edges);

function busca_recursiva($direcao,$rede,$id) {
	if (!in_array($id,$rede)) {
		$rede[] = $id;
	} else
		return $rede;

	//echo $id;exit;
	
	//Cadeia de composições:
	if ($direcao=='composicao' OR $direcao=='todos') {
		$sql = "SELECT id_empresa as id FROM relacoes WHERE id_empresaMae=$id AND id_empresa>0";
		$res = faz_query($sql,'','object');
		if ($res) {
			foreach ($res as $r) {
				$rede += busca_recursiva($direcao,$rede,$r->id);
			}
		}
	}
	
	//Cadeia de participações:
	if ($direcao=='participacao' OR $direcao=='todos') {
		$sql = "SELECT id_empresaMae as id FROM relacoes WHERE id_empresa=$id AND id_empresaMae>0";
		$res = faz_query($sql,'','object');
		if ($res) {
			foreach ($res as $r) {
				$rede += busca_recursiva($direcao,$rede,$r->id);
			}
		}
	}
	
	return $rede;
}

?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<HTML>
		<HEAD>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
            <title>Super mostrador de Rede</title>
            
            <script type="text/javascript" src="cytoscapeweb/js/json2.min.js"></script>
            <script type="text/javascript" src="cytoscapeweb/js/AC_OETags.min.js"></script>
            <script type="text/javascript" src="cytoscapeweb/js/cytoscapeweb.min.js"></script>
            
            
            <script type="text/javascript">
            
            function number_format(number, decimals, dec_point, thousands_sep) {
		    // http://kevin.vanzonneveld.net
		    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		    // +     bugfix by: Michael White (http://getsprink.com)
		    // +     bugfix by: Benjamin Lupton
		    // +     bugfix by: Allan Jensen (http://www.winternet.no)
		    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		    // +     bugfix by: Howard Yeend
		    // +    revised by: Luke Smith (http://lucassmith.name)
		    // +     bugfix by: Diogo Resende
		    // +     bugfix by: Rival
		    // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
		    // +   improved by: davook
		    // +   improved by: Brett Zamir (http://brett-zamir.me)
		    // +      input by: Jay Klehr
		    // +   improved by: Brett Zamir (http://brett-zamir.me)
		    // +      input by: Amir Habibi (http://www.residence-mixte.com/)
		    // +     bugfix by: Brett Zamir (http://brett-zamir.me)
		    // +   improved by: Theriault
		    var n = !isFinite(+number) ? 0 : +number, 
		        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		        s = '',
		        toFixedFix = function (n, prec) {
		            var k = Math.pow(10, prec);
		            return '' + Math.round(n * k) / k;
		        };
		    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
		    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		    if (s[0].length > 3) {
		        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		    }
		    if ((s[1] || '').length < prec) {
		        s[1] = s[1] || '';
		        s[1] += new Array(prec - s[1].length + 1).join('0');
		    }
		    return s.join(dec);
		}
            
                window.onload = function() {
                
				var meus_dados = {
					<?=$nodes?>,
					<?=$edges?>
				};
                
                
                
                    // id of Cytoscape Web container div
                    var div_id = "cytoscapeweb";
                    
                    // create a network model object
                    var network_json = {
                            // you need to specify a data schema for custom attributes!
                            dataSchema: {
                                nodes: [ { name: "label", type: "string" },
                                         { name: "ehControlada", type: "number" },
                                         { name: "ehControladoraUltima", type: "number" },
                                         { name: "ehParticipanteInterCadeias", type: "number" },
                                         { name: "tipoRanking", type: "text" },
                                         { name: "IPA", type: "number" },
                                         { name: "IPAtam", type: "number" },
                                         { name: "tipo", type: "string" },
                                         { name: "tipoTxt", type: "string" }
                                    ],
                                edges: [ { name: "weight", type: "number" },
			                             { name: "weightTxt", type: "string" },  
                                		 { name: "nameSource", type: "string" },
                                		 { name: "nameTarget", type: "string" }
                                ]
                            },
                            // NOTE the custom attributes on nodes and edges
                            data: meus_dados
                    };
                    
                    // initialization options
                   	var initOptions = {
	                   	swfPath: "cytoscapeweb/swf/CytoscapeWeb",
                        flashInstallerPath: "cytoscapeweb/swf/playerProductInstall",
                        resourceBundleUrl: "cytoscapeweb/texto.properties"
                    };
                    
                    
                var vis = new org.cytoscapeweb.Visualization(div_id, initOptions);
                    
                    // callback when Cytoscape Web has finished drawing
                    vis.ready(function() {
                    
                        // add a listener for when nodes and edges are clicked
                        vis.addListener("click", "nodes", function(event) {
                            handle_click(event);
                        })
                        .addListener("click", "edges", function(event) {
                            handle_click(event);
                        });
                        
                        function handle_click(event) {
                             var target = event.target;
                             
                             clear();
                             /*print("event.group = " + event.group);
                             for (var i in target.data) {
                                var variable_name = i;
                                var variable_value = target.data[i];
                                print( variable_name + " = " + variable_value );
                             }*/
                             if (event.group=="edges") {
                             	print( "<i>" + target.data['nameSource'] + "</i> tem <b>"+target.data['weightTxt']+"</b> da <i>" + target.data['nameTarget'] + "</i>");
                             } else {
                             	print( "<b>" + target.data['label'] + "</b> (id=" + target.data['id'] + ")");
                             	if (target.data['IPA']>0)
	                             	print( "Poder Acumulado (R$ milhões):<br>" + number_format(target.data['IPA'], 2, ',', '.') + "<br>");
                             	print( "Tipo: " + target.data['tipoTxt']);
                             	var ehControlada = (target.data['ehControlada']==0)
                             		? "não"
                             		: "sim";
                             	print( "É controlada? " + ehControlada );
                             	if (target.data['ehControladoraUltima']==1) {
	                             	print( "<i>Esta " + target.data['tipo'] + " é uma controladora última</i>" );
	                             }
	                            if (target.data['ehParticipanteInterCadeias']==1) {
	                             	print( "<i>Esta " + target.data['tipo'] + " é uma empresa com participações inter-cadeias</i>" );
	                             }
                             	print( "" );
                             	print( "<a href='mostra_rede.php?id=" + target.data['id'] + "'>Clique para ver sua rede de participações e de acionistas</a>" );
                             }
                        }
                        
                        function clear() {
                            document.getElementById("note").innerHTML = "";
                        }
                    
                        function print(msg) {
                            document.getElementById("note").innerHTML += "<p>" + msg + "</p>";
                        }
                    });
     
                    // draw options
                    var options = {
                    	network: network_json,
    				    nodeTooltipsEnabled: true,
    				    edgeTooltipsEnabled: true,
    				    panZoomControlVisible: true,
    				    edgesMerged: false,
    				    layout: {
							name: "ForceDirected",
							options: { }
						},
    				    visualStyle: {
    				        global: {
            				    backgroundColor: "#eee"
            				},
				            nodes: {
            				    /*color: { defaultValue: "#aaaaff", discreteMapper: { 
            				    	attrName: "id", entries: [ 
            				    		{ attrValue: <?=$id?>, value: "#f99" }
            				    	] }
            				    },*/
            				    size: { continuousMapper: { attrName: "IPAtam",  minAttrValue: 30, minValue: 30, maxValue: 100 } },
            				    labelYOffset: { continuousMapper: { attrName: "IPAtam",  minAttrValue: 30, minValue: 30, maxValue: 60 } },
            				    //labelYOffset: 20,
            				    color: { defaultValue: "#eee", discreteMapper: { 
            				    	attrName: "tipoRanking", entries: [ 
            				    		{ attrValue: "controladoraUltima", value: "#44f" }, 
            				    		{ attrValue: "participanteInterCadeias", value: "#4f4" }
            				    	] }
            				    }, 
            				    opacity: 0.9,
            				    borderWidth: 0,
            				    shape: "ELLIPSE",
            				    borderColor: "#707070",
            				    labelFontColor: "#000",
            				    hoverBorderWidth: 4,
            				    image: { defaultValue: "imgs/noun_project_3570_30x30.gif", discreteMapper: { 
            				    	attrName: "tipo", entries: [ 
            				    		{ attrValue: "Empresa", value: "imgs/noun_project_3570_30x30.gif" }, 
            				    		{ attrValue: "Pessoa", value: "imgs/noun_project_4738_30x30.gif" }, 
            				    		{ attrValue: "FocoEmpresa", value: "imgs/noun_project_3570_30x30_focoEmpresa.gif" },
            				    		{ attrValue: "FocoPessoa", value: "imgs/noun_project_4738_30x30_focoPessoa.gif" },  
            				    		{ attrValue: "Estado", value: "imgs/Estado.gif" }
            				    	] }
            				    }, 
            				    labelFontColor: "#009", 
            				    labelFontWeight: "bold", 
            				    tooltipText: { passthroughMapper: { attrName: "label" } }
            				    //labelHorizontalAnchor: "left"
            				},
            				edges: {
            					color: { continuousMapper: { attrName: "weight",  maxAttrValue: 50.01, minAttrValue: 50, minValue: "#666", maxValue: "#f00" } },
            					mergeColor: "#0b94b1",
            					width: { defaultValue: 2, continuousMapper: { attrName: "weight",  maxAttrValue: 50.01, minAttrValue: 0, minValue: 2, maxValue: 12 } },
            					style: { defaultValue: "SOLID", discreteMapper: { attrName: "weight",  entries: [
            						{ attrValue: 0, value: "DOT" },
            						{ attrValue: 0.01, value: "DOT" },
            						{ attrValue: 0.02, value: "DOT" },
            						{ attrValue: 0.03, value: "DOT" },
            						{ attrValue: 0.05, value: "DOT" } ] } },
            				    targetArrowShape: "ARROW", 
            				    tooltipText: { passthroughMapper: { attrName: "weightTxt" } }
            				 }
				        },
				        visualStyleBypass: {
				        	nodes: {
				        		"<?=$id?>": { shape: "HEXAGON", color: "#f00", size: 100 }
				        	}
				        }
    			};
                    
                vis.draw(options);
                
                var bypass = {
                	nodes: {
                		"<?=$id?>": {size: 100}
                	}, 
                	edges: {
                	}
                };
                vis.visualStyleBypass(bypass);
            };
            </script>
            
            <style>
                * { margin: 0; padding: 0; font-family: Helvetica, Arial, Verdana, sans-serif; }
                html, body { height: 100%; width: 100%; padding: 0; margin: 0; }
                body { line-height: 1.5; color: #000000; font-size: 14px; }
                /* The Cytoscape Web container must have its dimensions set. */
                #cytoscapeweb { width: <?=$largura?>px; height: 500px; }
                #note { float:right; width: 240px; height: 400px; background-color: #f0f0f0; overflow: auto;  }
                p { padding: 0 0.5em; margin: 0; }
                p:first-child { padding-top: 0.5em; }
            </style>
        </head>
        
        <body>
        	<b>Rede de participações e de acionistas da <?=$nome?> (id=<?=$id?>)</b><br>
        	<div id="note">
                <p>Você pode fazer zoom, mover as empresas e clicar sobre elas ou sobre suas ligações para receber informações.</p>
            </div>
            <div id="cytoscapeweb">
                Aguarde enquanto estou carregando a visualização de rede!
            </div>
        </body>
        
    </html>
