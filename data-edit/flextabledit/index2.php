<!DOCTYPE html>
<html lang="pt">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='http://fonts.googleapis.com/css?family=Ubuntu:300' rel='stylesheet' type='text/css' />
  <link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css' />
  
  <script src="/Content/Libs/modernizr-2.6.2/modernizr-2.6.2.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
  <script src="/Content/Libs/service.ontouchclick.jquery/service.ontouchclick.jquery.min.js"></script>
  
  <title>code.cwwonline - Demo: FlexTabledit</title>
  <link rel="stylesheet" type="text/css" href="src/service.dialog.jquery.css">
  <link rel="stylesheet" type="text/css" href="src/service.flatcheckbox.jquery.css">
  <link rel="stylesheet" type="text/css" href="src/service.flextabledit.jquery.css">
  <script src="src/service.flatcheckbox.jquery.min.js"></script>
  <script src="src/service.dialog.jquery.min.js"></script>

  <style>
    body {
      font-size: 15px;
    }

    .syntaxhighlighter {
      overflow-y: hidden !important;
      overflow-x: auto !important;
    }

    .myTable {
      border-collapse: collapse;
      border-spacing: 0;
      margin-top: 0.8333em;
      margin-bottom: 30px;
      width: 100%;
      background: #ffffff;
    }
    .myTable {
      background: #f4f6f5;
    }
    .myTable th:first-child,
    .myTable td:first-child {
      width: 32px;
      text-align: center;
    }
    .myTable th,
    .myTable td {
      border: 1px solid #b3bcba;
    }

  </style>

  


</head>

<body>
  <div class="pageWrapper">

    <div id="siteHeader">
      <div class="container">
        <div class="block">
          <a href="/" class="siteImage">&nbsp;</a>
          <span class="siteName">Software components for anyone interested, by Jos Huybrighs</span>
          <div class="right"></div>
        </div>
      </div>
    </div>

    <div id="tutorialContentWrapper">
      


<div class="content">
  <div class="container">
    <div id="clientSideList" class="block demo">
      <h1>FlexTabledit demo</h1>
      <p>
        You can find 2 example tables on this page:
        <ul>
          <li>The first one is a table with preconfigured content.</li>
          <li>The second is a completely empty table.</li>
        </ul>        
      </p>

      <h4>1. Preconfigured table</h4>
      <div id="carsTable"></div>
      <div style="border-bottom: 1px solid #497981; margin-bottom: 1.833em;">
        <input id="flextabledit1ChkBox" type="checkbox" />
        <div id="flextabledit1Tutorial" style="display: none; margin-bottom: 2.0em;">
          <h4>HTML and Javascript code</h4>

        </div>
      </div>


      <div id="createTableDialog" class="dialogPopUp">
        <div class="closeButton" onclick="javascript: $('#createTableDialog').dialog('close');">
          <span>X</span>
        </div>
        <h2>Initial table size</h2>
        <div class="sideBySideCols">
          <div class="leftCol-s" style="width:160px;">
            <p><b>Number of columns:</b></p>
          </div>
          <div class="rightCol">
            <input id="flxtblNrOfCols" size="4" value="4" />
          </div>
        </div>
        <div class="sideBySideCols">
          <div class="leftCol-s" style="width:160px;">
            <p><b>Number of rows:</b></p>
          </div>
          <div class="rightCol">
            <input id="flxtblNrOfRows" size="4" value="10" />
          </div>
        </div>
        <div class="button" style="margin-top:0.833em;" onclick="javascript: onCreateTable();">Create table</div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">

  function onAddTable() {
    $('#createTableDialog').dialog();
  }

  function onCreateTable() {
    var nrofCols = $('#flxtblNrOfCols').val();
    if ( nrofCols < 1) nrofCols = 1;
    var nrofRows = $('#flxtblNrOfRows').val();
    if ( nrofRows < 1) nrofRows = 1;
    nrofRows++;
    $('#createTableDialog').dialog('close');
    // Init table
    var content = [];
    for (var r = 0; r < nrofRows; r++ ) {
      var row = [];
      for (var c = 0; c < nrofCols; c++) {
        row.push("");
      }
      content.push(row)
    }
    $('#ownTableUnAvailPrompt').hide();
    $('#ownTableAvailPrompt').show();
    $('#ownTable').flextabledit({
      content: content,
      addTableClass: "myTable",
      texts: { cut: 'Couper', copy: 'Copier', paste: 'Coller', insert: 'Insérer', remove: 'Supprimer', columnName: 'Nom' }
    });
  }

  function onDeleteTable() {
    $('#ownTable').flextabledit('destroy');
    $('#ownTableAvailPrompt').hide();
    $('#ownTableUnAvailPrompt').show();
  }

  // Execute on page load
  $(function () {

    var contentArray = [
	    ["Year", "Ford", "Volvo", "Toyota", "Honda", "Remark"],
	    ["2014", 10, 11, 12, 13, '-'],
	    ["2015", 20, 11, 14, 13, "Boston's figures"],
	    ["2016", 30, 15, 12, 13, 'Checked: "7"']
    ];
    $('#carsTable').flextabledit({
      content: contentArray,
      addTableClass: "myTable"
    });

  });

</script>

    </div>

    <div id="tutorialFooterWrapper">
      <div class="footer">
        <div class="container">
          <div class="col s-span-12 m-span-7">
            <h3>Open Source Software Contributions</h3>
            <p>Contact: <a href="mailto:info@cwwonline.be" target="_blank">info@cwwonline.be</a></p>
          </div>
          <div class="col s-span-12 m-span-5">
            <div class="section collapse s_topline">
              <div class="col m-span-6">
                <h4>POWERED BY</h4>
                <a href="http://www.asp.net/mvc" target="_blank">ASP.NET MVC</a><br />
                <div class="poweredByUmbraco">
                  <a class="text" href="http://umbraco.com/" target="_blank">Umbraco</a>
                  &nbsp;
                  <a class="umbracoIcon" href="/umbraco" target="_blank"></a>
                </div>
              </div>
              <div class="col m-span-6">
                <h4>INTERESTING LINKS</h4>
                <a href="http://www.github.com/" target="_blank">GitHub</a><br />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</body>

</html>
