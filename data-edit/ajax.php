<?php
include('../config.php');
include('../funcoes_comuns.php');
if (isset($_POST['data'])) {
	$arq = fopen('../'.$config->arq_db, 'w');
	if (($handle = fopen('../'.$config->arq_db, "r")) !== FALSE) {
		$data = json_decode($_POST['data']);
		foreach($data as $row) {
			fputcsv($arq, $row, "\t");
		}
		echo 'SALVO!';
	} else {
		echo 'ERRO!';
	}
	fclose($arq);
} else {
	$data = array();
	if (($handle = fopen('../'.$config->arq_db, "r")) !== FALSE) {
		while (($d = fgetcsv($handle, 10000, "\t")) !== FALSE) {
			$data[] = $d;
		}
	}
	fclose($handle);
	header('Content-Type: application/json');
	echo json_encode($data);
}

?>

