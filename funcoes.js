jQuery( document ).ready(function() {
	/*jQuery(".filter").click( function(){
		var checkBox = jQuery("#"+this.id+" :first-child");
		checkBox.prop("checked", !checkBox.prop("checked"));
		jQuery("#form_filtros").submit();
	});*/
	jQuery("#compartilhar").click( function(){
		var link = url+"?vinculo="+vinculo;
		var menus = ['atores', 'produtos', 'sites_base'];
		var checked = [];
		jQuery.each(menus, function (i,v) {
			checked = jQuery( "#form_filtros #"+v+" input:checked" );
			if (checked.length>0) {
				jQuery.each(checked, function(ii,vv) {
					link+="&c_"+v+"[]="+vv.value;
				});
			}
		});
		jQuery("#compartilhar_span")
			.html(link)
			.toggle('slow');
	});
	jQuery("#compartilhar_span").click( function(){
		selectText("compartilhar_span");
	});
	/*jQuery("#form_filtros input").change( function(){
		var ar = [];
		var ref = this.value;
		var name = this.name;
		jQuery.each(vis.nodes(), function (i,v) {
			if (name=='c_atores' && v.data.tipo==ref) {
				ar.push(v.data.id);
			} else if (name=='c_produtos' && vinculo=='produto' && v.data.tipo=='produto' && jQuery.parseJSON(v.data['extra']).title==ref) {
				ar.push(v.data.id);
			} else if (name=='c_produtos' && vinculo!='produto' && v.data.tipo=='url') {
				var prods=jQuery.parseJSON(v.data['extra']).produtos_biofortificados.replace(/-/g,' ').split(',');
				if (jQuery.inArray(ref, prods)>=0)
					ar.push(v.data.id);
			} else if (name=='c_sites_base' && v.data.tipo=='url' && v.data.label.substring(0,v.data.label.length-6)==ref) {
				ar.push(v.data.id);
			}
		});
		if (this.checked)
			vis.select('nodes',ar);
		else
			vis.deselect('nodes',ar);
	});
	jQuery("#form_vinculos select").change( function(){
		jQuery("#form_vinculos").submit();
	});
	jQuery("#form_filtros").submit(function( event ) {
		vis.removeFilter();
		vis.deselect('nodes');
		aplica_todos_filtros();
		event.preventDefault();
	});
	*/
});

function aplica_todos_filtros() {
	var menus = jQuery(".filter_ul");
	var filtros = [];
	var checked = [];
	var tmp = [];
	jQuery.each(menus, function (i,v) {
		tmp = [];
		checked = jQuery( "#"+v.id+" input:checked" );
		if (checked.length>0) {
			jQuery.each(checked, function(ii,vv) {
				tmp.push(vv.value);
			});
			filtros.push(['filter-'+v.id,tmp]);
		}
	});
	if (filtros.length>0) {
		aplica_filtro(filtros);
	}
	vis.layout('ForceDirected');
}

function aplica_filtro(filtros) {
	var field = "";
	var fieldFilters = [];
	var nodesToShow = [];
	var allNodes = vis.nodes();
	var found = false;
	for (var n=0; n<allNodes.length; n++) {
		found = false;
		for (var i=0; i<filtros.length; i++) {
			field = filtros[i][0];
			fieldFilters = filtros[i][1];
			targetValues = allNodes[n].data[field].split('|');
			for (var j=0; j<targetValues.length; j++) {
				for (var k=0; k<fieldFilters.length; k++) {
					if (targetValues[j]==fieldFilters[k]) {
						nodesToShow.push(allNodes[n].data.id);
						nodesToShow.push(allNodes[n].data.parents.split('|'));
						found = true;
						break;
					}
				}
				if (found) {
					break;
				}
				/*if (jQuery.inArray(targetValues[j], fieldFilters)>=0) {
					
				}*/
			}
			if (found) {
				break;
			}
		}
	}
	vis.filter('nodes', nodesToShow, false);
	
	/*
	vis.filter(
		'nodes', 
		function(node) {
			//if (node.data.tipo!='url') {
				n = vis.firstNeighbors([node],true);
				var done = false;
				jQuery.each(n.neighbors, function(i,v) {
					if (jQuery.inArray(node.data[cat], filtros)>=0) {
						done = true;
						return false;
					}
				});
				return done;
			//} else {
				// console.log(node.data.parents.split('|'));
				var node_filters = node.data[cat].split('|');
				for (i=0; i<node_filters.length; i++) {
					if (jQuery.inArray(node_filters[i], filtros)>=0) {
						return true;
					}
				}
				return false;
			//}
		}, 
		false
	);*/
}

function number_format(number, decimals, dec_point, thousands_sep) {
		    // http://kevin.vanzonneveld.net
		    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		    // +     bugfix by: Michael White (http://getsprink.com)
		    // +     bugfix by: Benjamin Lupton
		    // +     bugfix by: Allan Jensen (http://www.winternet.no)
		    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		    // +     bugfix by: Howard Yeend
		    // +    revised by: Luke Smith (http://lucassmith.name)
		    // +     bugfix by: Diogo Resende
		    // +     bugfix by: Rival
		    // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
		    // +   improved by: davook
		    // +   improved by: Brett Zamir (http://brett-zamir.me)
		    // +      input by: Jay Klehr
		    // +   improved by: Brett Zamir (http://brett-zamir.me)
		    // +      input by: Amir Habibi (http://www.residence-mixte.com/)
		    // +     bugfix by: Brett Zamir (http://brett-zamir.me)
		    // +   improved by: Theriault
		    var n = !isFinite(+number) ? 0 : +number, 
		        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		        s = '',
		        toFixedFix = function (n, prec) {
		            var k = Math.pow(10, prec);
		            return '' + Math.round(n * k) / k;
		        };
		    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
		    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		    if (s[0].length > 3) {
		        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		    }
		    if ((s[1] || '').length < prec) {
		        s[1] = s[1] || '';
		        s[1] += new Array(prec - s[1].length + 1).join('0');
		    }
		    return s.join(dec);
		}

function selectText(element) {
    var doc = document
        , text = doc.getElementById(element)
        , range, selection
    ;    
    if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) { //all others
        selection = window.getSelection();        
        range = doc.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}
