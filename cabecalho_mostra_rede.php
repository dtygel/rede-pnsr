<?php

	$largura = (isset($_REQUEST['largura']))
		? $_REQUEST['largura']
		: '500';
	
	$dir = (isset($_REQUEST['dir']))
		? $_REQUEST['dir']
		: 'todos';
		
	if ($incorporado)
		$linkPara = "&incorporado=1";
	
	// Opções da rede a ser gerada:
	$vinculo = (isset($_REQUEST['vinculo']))
		? $_REQUEST['vinculo']
		: $config->default_bond;
	$opcoes = array(
		'vinculo'=>$vinculo
	);
	
	// A rede:
	$rede = le_csv_nodes_e_edges($opcoes);
	$nodes_json = "nodes: ".json_encode($rede['nodes']);
	$edges_json = "edges: ".json_encode($rede['edges']);
	
	$node_keys = array();
	foreach (array_values($rede['nodes'])[0] as $key=>$r) {
		$novo = new StdClass();
		$novo->key = $key;
		$novo->type = ($key=='size') ? "number" : "string";
		$node_keys[] = $novo;
	}
?>
<script type="text/javascript" src="cytoscapeweb/js/json2.min.js"></script>
<script type="text/javascript" src="cytoscapeweb/js/AC_OETags.min.js"></script>
<script type="text/javascript" src="cytoscapeweb/js/cytoscapeweb.min.js"></script>
<script src="jquery.tools.min.js"></script>
<script src="funcoes.js"></script>
            
            <script type="text/javascript">
            
            	var vis = '';
            	var vinculo = '<?=$opcoes["vinculo"]?>';
            	var url = '<?="http://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]"?>';
            	
                window.onload = function() {
                
				var meus_dados = {
					<?=$nodes_json?>,
					<?=$edges_json?>
				};
                
                
                
                    // id of Cytoscape Web container div
                    var div_id = "map-canvas";
                    
                    // create a network model object
                    var network_json = {
                            // you need to specify a data schema for custom attributes!
                            dataSchema: {
                                nodes: [ 
                                <?php foreach ($node_keys as $node_key) { ?>
                                	{ name: "<?= $node_key->key ?>", type: "<?= $node_key->type ?>" },
                               	<?php } ?>
                                ],
                                /*edges: [ 
                                ]*/
                            },
                            data: meus_dados
                    };
                    
                   	var initOptions = {
	                   	swfPath: "cytoscapeweb/swf/CytoscapeWeb",
                        flashInstallerPath: "cytoscapeweb/swf/playerProductInstall",
                        resourceBundleUrl: "cytoscapeweb/texto.properties"
                    };
                    
                //Esta é a linha para desabilitar se estou querendo testar outras partes da página sem ficar tendo que carregar o cytoscapeweb!  
                vis = new org.cytoscapeweb.Visualization(div_id, initOptions);
                
                    
                    // callback when Cytoscape Web has finished drawing
                    vis.ready(function() {
                    	aplica_todos_filtros();
                    	vis.panEnabled(true);
                        // add a listener for when nodes and edges are clicked
                        vis.addListener("click", "nodes", function(event) {
                            handle_click(event);
                        })
                        .addListener("click", "edges", function(event) {
                            handle_click(event);
                        });
                        
                        function handle_click(event) {
                             var target = event.target;
                             
                             clear();
                             /*print("event.group = " + event.group);
                             for (var i in target.data) {
                                var variable_name = i;
                                var variable_value = target.data[i];
                                print( variable_name + " = " + variable_value );
                             }*/
                             if (event.group=="edges") {
                             	//print( "<i>" + target.data['nameSource'] + "</i> tem <b>"+target.data['weightTxt']+"</b> da <i>" + target.data['nameTarget'] + "</i>");
                             	//print( "<i>cliquei!</i>");
                             } else {
                             	var extra = jQuery.parseJSON(target.data['extra']);
                             	if (target.data['tipo'] == 'url') {
                             		print( "<h1>" + target.data['label'] + "</h1>");
                             		print( '<p><b>Tipo:</b> ' + target.data['tipo'] + '</p>');
                             		print( '<p><b>Link:</b> <a href="' + extra.url + '">'+extra.url+'</a></p>');
                             		if (extra.produtos_biofortificados.length>0)
                             			print( '<p><b>Produtos citados:</b> ' + extra.produtos_biofortificados + '</p>');
                             		if (extra.evento.length>0)
                             			print( '<p><b>Evento citado no link:</b> ' + extra.evento + '</p>');
                             		if (extra.ano.length>0)
                             			print( '<p><b>Ano:</b> ' + extra.ano + '</p>');
                             		if (extra.conteudo.length>0)
                             			print( '<p><b>Conteúdo do link:</b> ' + extra.conteudo + '</p>');
                             		if (extra.pais.length>0)
                             			print( '<p><b>País citado na notícia:</b> ' + extra.pais + '</p>');
                             	} else {
                             		if (target.data['html']) {
                             			jQuery("#note").html(target.data['html']);
                             		} else {
                             		jQuery("#note").html("<div id='loading'>carregando dados...</div>");
									var params = {
										f: 'detalhes',
										id: target.data['id'],
										vinculo: vinculo
									};
									jQuery.post('index.php', params, function(ret) {
										var html = "";
										html += "<h1>" + target.data['label'] + "</h1>";
                             			html += '<p><b>Tipo:</b> ' + target.data['tipo'] + '</p>';
										if (typeof extra.pais_de_origem!='undefined' && extra.pais_de_origem.length>0)
                             				html += '<p><b>País de origem:</b> ' + extra.pais_de_origem + '</p>';
                             			if (typeof extra.UF!='undefined' && extra.UF.length>0)
                             				html += '<p><b>UF:</b> ' + extra.UF + '</p>';
                             			if (ret.produtos!='') {
											html += "<h2 class='produtos' onClick='jQuery(\"ul.produtos\").toggle(400)'>Produtos relacionados (ver)</h2>";
											html += "<ul class='produtos'>" + ret.produtos + "</ul>";
										}
										if (ret.atores!='') {
											html += (vinculo=='url')
												? "<h2 class='atores' onClick='jQuery(\"ul.atores\").toggle(400)'>Outros atores citados nos seus links diretos (ver)</h2>"
												: "<h2 class='atores' onClick='jQuery(\"ul.atores\").toggle(400)'>Atores relacionados (ver)</h2>";
											html += "<ul class='atores'>" + ret.atores + "</ul>";
										}
										if (ret.links!='') {
											html += "<h2 class='links' onClick='jQuery(\"ul.links\").toggle(400)'>Notícias relacionadas (ver)</h2>";
											html += "<ul class='links'>" + ret.links + "</ul>";
										}
										jQuery("#note").html(html);
										target.data['html']=html;
										vis.updateData([target]);
									},"json");
									}
                             	}
                             	
                             }
                        }
                        
                        function clear() {
                            document.getElementById("note").innerHTML = "";
                        }
                    
                        function print(msg) {
                            document.getElementById("note").innerHTML += "<p>" + msg + "</p>";
                        }
                    });
     
                    // draw options
                    var options = {
                    	network: network_json,
    				    nodeTooltipsEnabled: true,
    				    edgeTooltipsEnabled: true,
    				    panZoomControlVisible: true,
    				    edgesMerged: false,
    				    layout: {
							name: "ForceDirected",
							//name: "CompoundSpringEmbedder", 
							options: { }
						},
    				    visualStyle: {
    				        global: {
            				    backgroundColor: "#fff"
            				},
				            nodes: {
            				    size: { passthroughMapper: { attrName: "size" } },
            				    labelYOffset: { passthroughMapper: { attrName: "size" } }, 
            				    color: { defaultValue: "#eee"/*, discreteMapper: { 
            				    	attrName: "icon", entries: [ 
            				    		{ attrValue: "governo-federal", value: "#fff" }, 
            				    		{ attrValue: "governo-estadual", value: "#fff" }, 
            				    		{ attrValue: "sociedade-civil", value: "#00f" }, 
            				    		{ attrValue: "articulacao", value: "#f44" },
            				    		{ attrValue: "rede", value: "#f44" },  
            				    	] }*/
            				    }, 
            				    selectionColor: "#0f0",
            				    opacity: 0.9,
            				    borderWidth: 0,
            				    shape: "ELLIPSE",
            				    borderColor: "#707070",
            				    selectionBorderColor: "#070", 
            				    selectionGlowColor: "#070",
            				    labelFontColor: "#000",
            				    hoverBorderWidth: 4,
            				    image: { defaultValue: "imgs/<?= $options->icons->icons[to_title($config->default_icon)] ?>", discreteMapper: { 
            				    	attrName: "icon", entries: [ 
            				    		<?php foreach ($options->icons->icons as $icon_attr=>$icon_img) { ?>
            				    			{ attrValue: "<?= $icon_attr ?>", value: "imgs/<?= $icon_img ?>" }, 
            				    		<?php } ?>
            				    	] }
            				    }, 
            				    labelFontColor: "#009", 
            				    labelFontWeight: "bold", 
            				    tooltipText: { passthroughMapper: { attrName: "fullname" } }
            				    //labelHorizontalAnchor: "left"
            				},
            				edges: {
            					/*color: { continuousMapper: { attrName: "weight",  maxAttrValue: 50.01, minAttrValue: 50, minValue: "#666", maxValue: "#f00" } },*/
            					color: "#666", 
            					mergeColor: "#0b94b1",
            					width: 2, 
            					/*width: { defaultValue: 2, continuousMapper: { attrName: "weight",  maxAttrValue: 50.01, minAttrValue: 0, minValue: 2, maxValue: 12 } },*/
            					style: "SOLID", 
            				    targetArrowShape: "ARROW",
            				    //tooltipText: { passthroughMapper: { attrName: "" } }
            				 }
				        }
    			};
                    
                vis.draw(options);
            };
            </script>
            <script type="text/javascript">
			function mostraRede(id) {
				var janelaRedeVisual=window.open('mostra_rede.php?id='+id,'','height=600,width=800,toolbar=no,menubar=no,location=no,status=no,scrollbars=yes','mostrarede');
				if (window.focus) {janelaRedeVisual.focus()}
			}
</script>
<?php

?>
