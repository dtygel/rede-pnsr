<?php

function get_data_from_google_drive() {
	global $config;
	
	require_once 'vendor/autoload.php';

	session_start();

	$client = new Google_Client();
	$client->setAuthConfigFile($config->client_secrets);
	$client->addScope(Google_Service_Sheets::SPREADSHEETS_READONLY);

	if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
		$client->setAccessToken($_SESSION['access_token']);
		
		if ($client->isAccessTokenExpired()) {
			$redirect_uri = $config->dir['oauth2callback'];
			header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
		}
		
		$service = new Google_Service_Sheets($client);
		$spreadsheetId = '1PpDCgL7eIj4SyFV94ILQN5Wpev7ofC9-qW30O_-PdMc';
		$range = 'Atores!A1:O';
		$response = $service->spreadsheets_values->get($spreadsheetId, $range);
		$rows = $response->getValues();

		if (count($rows) == 0) {
			return false;
		}
		$data = array();		
		foreach ($rows as $i=>$row) {
			if ($i==1) {
				continue;
			}
			$data[] = $row;
		}
		return $data;
		
	} else {
		$redirect_uri = $config->dir['oauth2callback'];
		header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
	}


}

?>
