<?php

require_once 'config.php';
require_once 'vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile($config->client_secrets);
$client->setRedirectUri($config->dir['oauth2callback']);
$client->addScope(Google_Service_Sheets::SPREADSHEETS_READONLY);

if (! isset($_GET['code'])) {
  $auth_url = $client->createAuthUrl();
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  $redirect_uri = $config->dir['gera_csv_nodes_e_edges'];
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

?>
