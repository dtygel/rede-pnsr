<?php

include('config.php');
include('funcoes_comuns.php');

$action = $_POST['action'];
$data = $_POST['data'];
$filename = $_POST['filename'];

if ($action=='save_structure') {
	if ($data && $filename) {
		$finalData = new StdClass();
		$finalData->name = $filename;
		$finalData->data = array();
		foreach ($data as $d) {
			$tmp = new StdClass();
			$tmp->id = $d['id'];
			$tmp->position = new StdClass();
			$tmp->position->x = floatval($d['position']['x']);
			$tmp->position->y = floatval($d['position']['y']);
			$finalData->data[] = $tmp;
		}
		if (!file_put_contents("dados/estruturas/$filename.json", json_encode($finalData))) {
			$ret = new StdClass();
			$ret->success = 0;
			$ret->msg = "I failed creating and saving data to file '$filename.json'";
		} else {
			$ret = new StdClass();
			$ret->success = 1;
			$ret->msg = "";
		}
		header('Content-type: application/json');
		echo json_encode($ret);
	}
	exit;
} elseif ($action=='remove_structure') {
	if ($filename) {
		$ret = new StdClass();
		if (unlink($config->dir_rel['estruturas']."$filename.json")) {
			$ret->success = 1;
			$ret->msg = "Successfully removed file $filename.json";
		} else {
			$ret->success = 0;
			$ret->msg = "Failed removing file $filename.json! Error: \"".json_encode(error_get_last())."\"";
		}
		header('Content-type: application/json');
		echo json_encode($ret);
	}
	exit;
} elseif ($action=='bookmark_structure') {
	if ($filename) {
		$ret = new StdClass();
		if (copy($config->dir_rel['estruturas']."$filename.json", $config->dir_rel['estruturas']."default.json")) {
			$ret->success = 1;
			$ret->msg = "Successfully bookmarked $filename as default structure.";
		} else {
			$ret->success = 0;
			$ret->msg = "Failed bookmarking file $filename as default structure! Error: \"".json_encode(error_get_last())."\"";
		}
		header('Content-type: application/json');
		echo json_encode($ret);
	}
	exit;
}

$estrutura_padrao = le_estrutura('default');
$estrutura_padrao = $estrutura_padrao->name;

$structuresRaw = scandir($config->dir_rel['estruturas']);
$structures = array();
foreach ($structuresRaw as $structure) {
	if (!in_array($structure, array("default.json", "..", "."))) {
		$structures[] = basename($structure, ".json");
	}
}



?>

<div id="manageStructuresWrapper">
	<form id="addCustomStructure" autocomplete="off">
		<div class="form-group">
		  <label for="manageStructuresInputLabel">Nome para esta estrutura</label>
		  <input type="text" class="form-control" id="manageStructuresInput" aria-describedby="manageStructuresInputHelp" placeholder="Nome do arquivo">
		  <small id="manageStructuresInputHelp" class="form-text text-muted">Escolha um nome que te faça lembrar desta estrutura.</small>
		</div>
		<button type="submit" id="manageStructuresSubmit" class="disabled btn btn-primary">Salvar estrutura atual</button>
	</form>
	
	<script>
		var structures = ['<?= implode("','", $structures) ?>'];
		$('#manageStructuresInput').keyup(function(){
			$('#manageStructuresSubmit').toggleClass('disabled', $(this).val().length == 0);
		})
		$("#addCustomStructure").submit(function( event ) {
			var filename = $("#manageStructuresInput").val();
			var found = false;
			$.each(structures, function ( i, val) { 
				if (filename == val) {
					alert("Uma estrutura com este nome já existe. Favor alterar o nome ou apague antes a estrutura que já existe.");
					found = true;
					return false;
				}
			});
			if (!found) {
				var data = [];
				cy.nodes().forEach(function( ele ){
					data.push({'id': ele.id(), 'position': ele.position() });
				});
				var request = $.ajax({
					url: "manage_custom_structures.php",
					method: "POST",
					data: {
						'action' : 'save_structure',
						'filename' : filename,
						'data' : data
					}
				})
					.done(function () {
						$("#manageStructuresWrapper").html(''+
							'<div class="alert alert-success">' +
								'<p>A estrutura <b>'+filename+'</b> foi salva com sucesso.</p>' +
							'</div>');
						$("#structure-name span").html(filename);
						setTimeout(function(){ dialog.close(); }, 1000);
					});
			}
		
			event.preventDefault();
		});
		
	</script>

	<hr />
	
	<h3>Estruturas armazenadas</h3>
	<table class="table">
		<thead>
			<tr>
				<th>Ações</th>
				<th>Estrutura</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($structures as $s) {
				if ($s==$estrutura_padrao) {
					$btn_bookmark = "btn-success";
					$disabled=" disabled";
				} else {
					$btn_bookmark = "btn-default";
					$disabled="";
				} ?>
					<tr id="row_<?= $s ?>">
						<td>
							<span action="remove_structure" class="btn btn-danger btn-xs glyphicon glyphicon-remove<?= $disabled ?>" target="<?= $s ?>" data-toggle="tooltip" title="Remover retrato"></span> <span action="open_structure" class="btn btn-default btn-xs glyphicon glyphicon-folder-open" target="<?= $s ?>" data-toggle="tooltip" title="Ver retrato"></span> <span action="bookmark_structure" class="btn <?= $btn_bookmark ?> btn-xs glyphicon glyphicon-bookmark<?= $disabled ?>" target="<?= $s ?>" data-toggle="tooltip" title="Ver e definir esta estrutura como padrão">
						</td>
						<td>
							<?= $s ?>
						</td>
					</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<script>
	$('[data-toggle="tooltip"]').tooltip();
	$("span[action$='_structure']").click(function() {
	var action = $(this).attr('action');
	var target = $(this).attr('target');
	if (action=='open_structure') {
		dialog.close();
		window.open("?estrutura="+target,"_self");
	}
	var request = $.ajax({
					url: "manage_custom_structures.php",
					method: "POST",
					data: {
						'action' : action,
						'filename' : target
					}
				})
					.done(function () {
						if (action=='remove_structure') {
							$.each(structures, function ( i, val) { 
								if (val==target) {
									structures.splice(i);
									return false;
								}
							});
							$("#row_"+target).hide(500);
						} else if (action=='bookmark_structure') {
							dialog.close();
							window.open("?estrutura="+target,"_self");
						}
					});
	});
</script>
