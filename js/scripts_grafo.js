var dialog;
var cy;
var cachedData = [];
var layoutPadding = 50;
var layoutDuration = 500;

var infoTemplate = Handlebars.compile([
	  '<p class="ac-name">{{nome-do-ator}} {{#if sigla}}({{sigla}}){{/if}}</p>',
	  '<p class="ac-node-type"><i class="glyphicon glyphicon-list"></i> {{tipo-de-ator}} {{#if Type}}({{Type}}){{/if}}</p>',
	  '{{#if indicacao-de-territorios}}<p class="ac-milk"><i class="glyphicon glyphicon-globe"></i> {{indicacao-de-territorios}}{{/if}} {{#if abrangencia}}({{abrangencia}}){{/if}}</p>',
	  '{{#if forma-de-atuacao-relacionada-ao-saneamento-rural}}<p class="ac-country"><i class="glyphicon glyphicon-star-empty"></i> <span class="ac-label">Forma de atuação:</span> {{forma-de-atuacao-relacionada-ao-saneamento-rural}}</p>{{/if}}',
	  '{{#if ordem-de-grandeza-da-quantidade-de-pessoas-envolvidas}}<p class="ac-country"><i class="glyphicon glyphicon-star-empty"></i> <span class="ac-label">Pessoas envolvidas:</span> {{ordem-de-grandeza-da-quantidade-de-pessoas-envolvidas}}</p>{{/if}}',
	  '{{#if ordem-de-grandeza-de-seu-orcamento-anual}}<p class="ac-country"><i class="glyphicon glyphicon-star-empty"></i> <span class="ac-label">Orçamento anual:</span> {{ordem-de-grandeza-de-seu-orcamento-anual}}</p>{{/if}}',
	  '{{#if publico-ou-tipo-de-territorio-com-o-qual-trabalha-ou-o-qual-representa}}<p class="ac-country"><i class="glyphicon glyphicon-star-empty"></i> <span class="ac-label">Público com o qual trabalha:</span> {{publico-ou-tipo-de-territorio-com-o-qual-trabalha-ou-o-qual-representa}}</p>{{/if}}',
	  '{{#if relacao-com-a-construcao-do-pnsr}}<p class="ac-country"><i class="glyphicon glyphicon-star-empty"></i> <span class="ac-label">Relação com a construção do PNSR:</span> {{relacao-com-a-construcao-do-pnsr}}</p>{{/if}}',
	  '{{#if visao-com-relacao-ao-programa}}<p class="ac-country"><i class="glyphicon glyphicon-star-empty"></i> <span class="ac-label">Visão sobre o PNSR:</span> {{visao-com-relacao-ao-programa}}</p>{{/if}}'
	].join(''));


// ON PAGE LOAD:

$(function () {
	$('[data-toggle="tooltip"]').tooltip();

	$('#manage_custom_structures').click(function() {
		dialog = BootstrapDialog.show({
          title: 'Gestão de estruturas',
          message: function(dialog) {
              var $message = $('<div></div>');
              var pageToLoad = dialog.getData('pageToLoad');
              $message.load(pageToLoad);
              return $message;
          },
          data: {
              'pageToLoad': 'manage_custom_structures.php',
          }
    });
	});
	
	$('#reset').click(function(){
    cy.animate({
      fit: {
        eles: cy.elements(),
        padding: layoutPadding
      },
      duration: layoutDuration
    });
  });

	// TODO: Make the algorithm INTER filter fields (dropdown) be AND and not OR!!!! Now it's OR.
	var allFilterElements = $('#form_filtros input');
  allFilterElements.on('change', function(){
  	var el = $(this);
  	var field = 'filter-'+el.attr('name').substring(2);
  	var value = el.val();
  	var checked = el.is(':checked');
  	var noneSelectedInType = checkIfNoneSelected(el.attr('name'), value);
  	var selectedFilters = getSelectedFilters(allFilterElements);
  	
  	if (selectedFilters.length==0) {
  		cy.elements().removeClass('filtered');
  	} else {
  		cy.batch(function() {
  			cy.elements().addClass('filtered');
  			applyFilters(selectedFilters);
  		});
  	}
  		
  	cy.stop().animate({
      fit: {
        padding: layoutPadding
      }
    }, {
      duration: layoutDuration
    })
  });
  
});

// FUNCTIONS

function initCy(stylesCy) {
	cy = cytoscape({
		container: $('#map-canvas'),
		elements: elements,
		layout: layoutOptions,
		style: stylesCy
	});
	
	cy.on('select', 'node', function(e){
    var node = this;
    highlight( node );
    showNodeInfo( node );
  });
  
  cy.on('unselect', 'node', function(e){
    var node = this;
    clear();
    hideNodeInfo();
  });
  
  cy.on('mouseover', 'node', function(e) {
	  var title = e.cyTarget.data('fullname');
	  var label = e.cyTarget.data('label');
	  if (label && label!=title) {
	  	title += " ("+label+")";
	  }
    jQuery("#nodeTooltip")
    	.html(title)
      .css({
      	left:e.originalEvent.clientX-100,
      	top:e.originalEvent.clientY-50
			})
      .show();
	});
	
	cy.on('mouseout', 'node', function() {
		jQuery("#nodeTooltip")
			.hide();
	});
	
}

function applyFilters(selectedFilters) {
	var orderedFilters = {};
	var fields = [];
	$.each(selectedFilters, function (i, filterEl) {
		var field = 'filter-'+filterEl.attr('name').substring(2);
		if ($.inArray(field, fields)==-1) {
			fields.push(field);
			orderedFilters[field] = [filterEl.val()];
		} else {
			orderedFilters[field].push(filterEl.val());
		}
	});
	cy.nodes().forEach(function( n ){
		var applyFilter = true;
		$.each(orderedFilters, function (field, filterValues) {
			var applyFilterForThisField = false;
			$.each(filterValues, function (i, filterValue) {
				var nodeValues = n.data(field).split("|");
				if ($.inArray(filterValue, nodeValues)>-1) {
					applyFilterForThisField = true;
					return false;
				}
			});
			if (!applyFilterForThisField) {
				applyFilter = false;
				return false; // break loop among fields since this node is not in any filter of THIS field.
			}
		});
		if (applyFilter) {
			var nhood = n.closedNeighborhood();
			nhood.removeClass('filtered');
		}
	});
}

function applyFiltersOr(filters, action) {
	cy.nodes().forEach(function( n ){
		$.each(filters, function (i, filterEl) {
			var field = 'filter-'+filterEl.attr('name').substring(2);
			var nodeValues = n.data(field).split("|");
			if ($.inArray(filterEl.val(), nodeValues)>-1) {
				var nhood = n.closedNeighborhood();
				if (action=='check') {
					nhood.removeClass('filtered');
				} else {
					nhood.addClass('filtered');
				}
			}
		});
	});
}

function checkIfNoneSelected(field, valueToIgnore) {
	var filters = $("[name='"+field+"']");
	noneSelected = true;
	$.each(filters, function(i, filterEl) {
		var el = $(filterEl);
		filterValue = el.val();
		if(filterValue != valueToIgnore && el.is(':checked')) {
			noneSelected = false;
			return false;
		}
	});
	return noneSelected;
}

function getSelectedFilters(filters) {
	selectedFilters = [];
	$.each(filters, function(i, filterEl) {
		var el = $(filterEl);
		if(el.is(':checked')) {
			selectedFilters.push(el);
		}
	});
	return selectedFilters;
}

function highlight( node ){
  var nhood = node.closedNeighborhood();
  cy.batch(function(){
    cy.elements().not( nhood ).removeClass('highlighted').addClass('faded');
    nhood.removeClass('faded').addClass('highlighted');
    var npos = node.position();
    var w = window.innerWidth;
    var h = window.innerHeight;
    cy.stop().animate({
      fit: {
        eles: cy.elements(),
        padding: layoutPadding
      }
    }, {
      duration: layoutDuration
    }).delay( layoutDuration, function(){
      nhood.layout({
        name: 'concentric',
        padding: layoutPadding,
        animate: true,
        animationDuration: layoutDuration,
        boundingBox: {
          x1: npos.x - w/3,
          x2: npos.x + w/3,
          y1: npos.y - w/3,
          y2: npos.y + w/3
        },
        fit: true,
        concentric: function( n ){
          if( node.id() === n.id() ){
            return 2;
          } else {
            return 1;
          }
        },
        levelWidth: function(){
          return 1;
        }
      });
    } );
  });
}

function clear(){
  cy.batch(function(){
    cy.$('.highlighted').forEach(function(n){
      n.animate({
        position: n.data('position')
      });
    });
    
    cy.elements().removeClass('highlighted').removeClass('faded');
  });
}

function showNodeInfo( node ){
	var htmlObject = {};
	$.each(cachedData, function (i, val) {
		if (val.id == node.data('id')) {
			htmlObject = val.data;
			return false;
		}
	});
	if (htmlObject.length>0) {
		$('#info').html( infoTemplate( htmlObject ) ).show();
		return true;
	}
	$.ajax( "dados/rede-pnsr.json" ).done(function(data) {
			$.each(data, function (i, val) {
				if (val.id == node.data('id')) {
					cachedData.push({'id':node.data('id'), 'data': val });
					$('#info').html( infoTemplate( val ) ).show();
					return false;
				}
			});
		});
}

function hideNodeInfo(){
  $('#info').hide();
}
