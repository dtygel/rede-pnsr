<?php
?>
<link rel="stylesheet" type="text/css" href="data-edit/flextabledit/src/service.dialog.jquery.css" />
<link rel="stylesheet" type="text/css" href="data-edit/flextabledit/src/service.flextabledit.jquery.css" />
<script src="data-edit/flextabledit/src/service.flextabledit.jquery.min.js"></script>
<script src="data-edit/flextabledit/src/service.dialog.jquery.min.js"></script>

<style>
    .editableTable {
      border-collapse: collapse;
      border-spacing: 0;
      margin-top: 0.8333em;
      margin-bottom: 30px;
      width: 100%;
      background: #f4f6f5;
    }
    .editableTable th:first-child,
    .editableTable td:first-child {
      text-align: center;
    }
    .editableTable th,
    .editableTable td {
      border: 1px solid #b3bcba;
    }
</style>

<script type="text/javascript">
  $(function () {
  	$.post("data-edit/ajax.php", function( data ) {
  		console.log(data);
  		
  		$('#editableTable').flextabledit({
		    content: data,
		    addTableClass: "editableTable"
		  });
		  
		  jQuery('td').on('change', function (evt) {
				var contentArray = $('#editableTable').flextabledit('getData');
				if (contentArray.length != 0) {
					var jsonContent = JSON.stringify(contentArray);
					// Save content
					console.log(jsonContent);
					$.post( "data-edit/ajax.php", {data:jsonContent}, function( data ) {
						console.log(data);
					});
				}
			});
  	});
    
  });
</script>

<?php
