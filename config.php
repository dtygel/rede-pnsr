<?php
$config = new StdClass();
$config->debug = false;

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set("display_errors", "1");
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}

$charset='utf-8';
$lingua='pt-BR';
$senha='umcoracaoentremontanhas';
$senha2='freeasinbeer';
include('config_maquina_local.php');

$config->cytoscapejs = true; // if false, then we use the deprecated cytoscapeweb

$config->site = new StdClass();
$config->site->title = 'Rede de relações do Saneamento Rural';

if ($config->cytoscapejs) {
	$config->cabecalho_mostra_rede = 'cabecalho_mostra_rede_cytoscapejs.php';
} else {
	$config->cabecalho_mostra_rede = 'cabecalho_mostra_rede.php';
}

$config->arq_db = 'dados/rede-pnsr.tsv';
$config->arq_cache = 'dados/rede-pnsr.json';
$config->bond_types = array(
	"parents"
);
$config->default_size = 10;
$config->default_parent_size = 30;
$config->default_icon = "Movimento Social";
$config->default_parent_icon = "Articulação";
$config->default_bond = 'parents';

$config->fields = array(
	array(
		"label"=>"Data",
		"type"=>"date"
	),
	array(
		"label"=>"Sigla",
		"type"=>"shortname"
	),
	array(
		"label"=>"Nome do ator",
		"type"=>"label"
		//"filter"=>"true"
	),
	array(
		"label"=>"Abrangência",
		"multiple"=>"true"
		//"filter"=>"true"
	),
	array(
		"label"=>"Indicação de territórios",
		"multiple"=>"true"
	),
	array(
		"label"=>"Tipo de ator",
		"menu_label"=>"Ator",
		"filter"=>"true",
		"icons"=>array(
			"Academia"=>"academia.png",
			"Agência de regulação"=>"",
			"Arena"=>"redes.png",
			"Articulação"=>"redes.png",
			"Associação de empresas"=>"redes.png",
			"Autarquia federal"=>"governos.png",
			"Banco de fomento"=>"bancos.png",
			"Conselho Social"=>"conselho.png",
			"Empresa mista"=>"empresas.png",
			"Empresa privada"=>"",
			"Empresa pública"=>"governos.png",
			"Fundação pública"=>"governos.png",
			"Governo Federal"=>"governos.png",
			"Ministério"=>"",
			"Movimento Social"=>"sociedade-civil.png",
			"Organização Social"=>"sociedade-civil.png",
			"Política pública"=>"",
			"Programa"=>"",
			"Projeto"=>"",
			"Rede de organizações da sociedade civil"=>"redes.png",
			"Secretaria ministerial"=>"",
			"Sindical"=>"sociedade-civil.png",
			"Universidade"=>""
		)
	),
	array(
		"label"=>"Modalidades",
		"menu_label"=>"Modalidades",
		"multiple"=>"true",
		"filter"=>"true"
	),
	array(
		"label"=>"Público ou tipo de território com o qual trabalha ou o qual representa",
		"menu_label"=>"Público",
		"filter"=>"true"
	),
	array(
		"label"=>"Ordem de grandeza da quantidade de pessoas envolvidas",
		"menu_label"=>"Qtde pessoas"
	),
	array(
		"label"=>"Arenas ou articulações das quais participa",
		"menu_label"=>"Arena",
		"multiple"=>"true",
		"filter"=>"true",
		"parent"=>"true",
		"edge"=>"true",
		"private"=>"true"
	),
	array(
		"label"=>"Programas em que participa da execução",
		"menu_label"=>"Programa",
		"multiple"=>"true",
		"filter"=>"true",
		"parent"=>"true",
		"edge"=>"true",
		"private"=>"true"
	),
	array(
		"label"=>"Ator do qual faz parte",
		"menu_label"=>"Ator pai",
		"filter"=>"true",
		"parent"=>"true",
		"edge"=>"true"
	),
	array(
		"label"=>"Orçamento total do programa",
		"menu_label"=>"Orçamento"
	),
	array(
		"label"=>"Fontes de recursos do programa",
		"menu_label"=>"Financiador",
		"multiple"=>"true",
		"filter"=>"true",
		"parent"=>"true",
		"edge"=>"true"
	),
	array(
		"label"=>"Visão com relação ao programa",
		"menu_label"=>"Visão programa"
	),
	array(
		"label"=>"Relação com a construção do PNSR",
		"menu_label"=>"Envolvimento PNSR"
	),
	array(
		"label"=>"Grandeza de resultados de busca"
		/*"type"=>"integer",
		"size"=>"true",
		"size_intervals"=>array(
			1000=>'10',
			10000=>'20',
			50000=>'30',
			100000=>'40'
		)*/
	)
);

$config->dir = array();
if ($maquinalocal) {
	$config->dir['home']="/var/www/html/grit/rede-pnsr/";
	$config->dir['url']="http://localhost/grit/rede-pnsr";
	$config->client_secrets = 'client_secrets.json';
} else {
	$config->dir['home']="/home/dtygel/public_html/rede-pnsr/";
	$config->dir['url']="http://dtygel.vps2.eita.org.br/rede-pnsr";
	$config->client_secrets = 'client_secrets_remote.json';
}
$config->url = $config->dir['url']."/index.php";
$config->dir['oauth2callback'] = $config->dir['url']."/oauth2callback.php";
$config->dir['gera_csv_nodes_e_edges'] = $config->url."?f=gera_csv";
$config->dir_rel['estruturas'] = "dados/estruturas/";
?>
