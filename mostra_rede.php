<?php

$divclasstxt = (!$incorporado || ($incorporado && $id))
	? "simpleTabsContent"
	: "conteudoRanking";

//Só mostra abas se for ver uma empresa
//if (!$incorporado || ($incorporado && $id)) {
?>
<script src="jquery.tools.min.js"></script>
<script src="funcoes.js"></script>


<div class="simpleTabs">
<ul class="simpleTabsNavigation">
	<li><a class='especifica' href="#grafico">Visão gráfica</a></li>
	<li><a class='especifica' href="#lista">Tabela completa</a></li>
</ul>

<?php
//} // fim do if ($id)




//if ($id) {
//ABA DE VISUALIZAÇÃO GRÁFICA DA REDE
?>
<div class="<?=$divclasstxt?>">
	<div id="compartilhar"><img src="imgs/share_icon.png"></div>
	<span id="compartilhar_span"></span>
	<form id="form_vinculos" method="GET">
		<h1 class="titulo">
			<?= $config->site->title ?>
		</h1>
	</form>
	<form id="form_filtros" method="GET">
		<div id="filtros">
			<?php foreach ($rede['filters'] as $filter_slug=>$filter) { ?>
				<div id="<?= $filter_slug ?>" class="menu_filtro">
					<a href="javascript:jQuery('#<?= $filter_slug ?> div').toggle(400);void(0);"><?=$filter->title?></a>
					<div>
						<?php
							foreach ($filter->data as $item_slug=>$item) {
								$checkedtxt = (is_array($_REQUEST["c_$filter_slug"]) && in_array($item, $_REQUEST["c_$filter_slug"]))
									? ' checked="checked"'
									: "";
							?>
								<p><input type="checkbox" name="c_<?=$filter_slug?>" value="<?= $item_slug ?>"<?= $checkedtxt ?>> <?= $item ?></p>
							<?php
							}
						?>
						<p><input type="submit" value="filtrar"></p>	
					</div>
				</div>
			<?php } ?>
		</div>
	</form>
	<div id="noteGeral">
		<div id="note">
		</div>
		<div id="dicas">
			<p><b>Dicas:</b></p>
			<p>1. Faça zoom usando a barra de controle no canto inferior direito do gráfico</p>
			<p>2. Mova os nós e clique sobre eles para ver informações detalhadas.</p>
			<p>3. Passe o mouse ou clique sobre as setas para ver a porcentagem de participação relativa.</p>
		</div>
	</div>
	<div id="cytoscapeweb">
		Aguarde enquanto a visualização de rede é carregada...
	</div>
</div>




<?php
//ABA DE VISUALIZAÇÃO EM LISTA DA REDE
?>

<div class="<?=$divclasstxt?>">
<h1 style="margin:0;padding:0;text-align:center">Tabela completa</h1>
<iframe width='100%' height='450' frameborder='0' src='https://docs.google.com/spreadsheet/pub?key=0AqP97VRq8KdDdGNDaUgweF9KaFJoQnZRaEgwekt4S0E&single=true&gid=0&output=html&widget=true'></iframe>
<!--<iframe width='100%' height='450' frameborder='0' src='https://docs.google.com/spreadsheet/pub?key=0AqP97VRq8KdDdGNDaUgweF9KaFJoQnZRaEgwekt4S0E&single=true&gid=0&output=html&widget=true'></iframe>-->
</div>


<?php


//} // fim do if ($id)



//if ($id || !$incorporado) {
?>

</div>

<script>
$(function() {
  // setup ul.tabs to work as tabs for each div directly under div.panes
  $("ul.simpleTabsNavigation").tabs("div.simpleTabs > div.simpleTabsContent");
});
</script>

<?php /* } */ ?>
