<?php
mb_internal_encoding("UTF-8");
require_once "../config.php";
require_once "../funcoes_comuns.php";
$con=conecta($bd);
mysql_select_db($bd['eita_proprietariosdobrasil']);
if ($_REQUEST['qtde']) {
	$res = faz_query("SELECT count(*) c FROM logs",'','object');
	$total = $res[0]->c;
	$demo = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'."<HTML>
<HEAD><META HTTP-EQUIV='Content-Type' CONTENT='text/html; charset=utf-8'><title>Ranking Proprietários do Brasil</title><link rel=\"stylesheet\" type=\"text/css\" href=\"../estilo_incorporado.css\" /></HEAD><BODY><div id='banner'>
	<p><b><center>Ajude a revelar os proprietários do Brasil!</center></b>
	</p><p><b>$total pessoas</b> já acessaram o ranking Proprietários do Brasil desde o dia 14/12/2012.</p>
	<p><b><center><a target='_BLANK' href='http://proprietariosdobrasil.org.br'>Acesse você também e veja quem são as empresas mais poderosas do país</a></center></b></p>
	<p>Se você gostar do projeto, ajude a desenvolvê-lo ainda mais.</p>
	<p><b><center><a target='_BLANK' href='http://catarse.me/pt/portalproprietariosdobrasil'>Assista ao vídeo e contribua com qualquer valor</a>.</center></b></p>
	<p>Somente com o seu apoio conseguiremos revelar os donos do Brasil!</p>
	</div>
	</BODY>
	</HMTL>";
	if ($_REQUEST['demo'])
		echo $demo;
	else 
		echo $total;
	exit;
}

if ($_REQUEST['empresa_mais_acessada']) {
	$res = faz_query("SELECT count(*) as qtde, b.nome, b.PA, b.IPA, a.id, b.r FROM `logs` a, assoc b WHERE a.id=b.id GROUP BY a.id ORDER BY qtde DESC",'','object');
	$nome = $res[0]->nome;
	$qtde = $res[0]->qtde;
	$PA = number_format($res[0]->PA,2,',','.');
	$R = $res[0]->r;
	$id = $res[0]->id;
	$link = "$url?f=mostra_rede&id_empresa=$id&aba=grafico";
	$demo = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'."<HTML>
<HEAD><META HTTP-EQUIV='Content-Type' CONTENT='text/html; charset=utf-8'><title>Ranking Proprietários do Brasil</title><link rel=\"stylesheet\" type=\"text/css\" href=\"../estilo_incorporado.css\" /></HEAD><BODY><div id='banner'>
	<p><b><center>Ajude a revelar os proprietários do Brasil!</center></b>
	</p><p><b>$qtde pessoas</b> visualizaram a rede de poder da <a href='$link' target='_blank'>$nome</a>, cujo Poder Acumulado é de $PA milhões de reais, no <n>ranking Proprietários do Brasil</b>, desde o dia 14/12/2012. (<a href='$link' target='_blank'>Clique aqui para ver sua rede de poder</a>)</p>
	<p><b><center><a target='_BLANK' href='http://proprietariosdobrasil.org.br'>Acesse você também e veja quem são as empresas mais poderosas do país</a></center></b></p>
	<p>Se você gostar do projeto, ajude a desenvolvê-lo ainda mais.</p>
	<p><b><center><a target='_BLANK' href='http://catarse.me/pt/portalproprietariosdobrasil'>Assista ao vídeo e contribua com qualquer valor</a>.</center></b></p>
	<p>Somente com o seu apoio conseguiremos revelar os donos do Brasil!</p>
	</div>
	</BODY>
	</HMTL>";
	if ($_REQUEST['demo'])
		echo $demo;
	else 
		echo json_encode(array(
			'nome'=>$nome, 
			'qtde'=>$qtde,
			'link'=>$link,
			'PA'=>$PA,
			'R'=>$R
			));
	exit;
}

if ($_REQUEST['empresa_randomica']) {
	$res = faz_query("SELECT b.nome, b.PA, b.IPA, b.id, b.r FROM assoc b WHERE PA>0 AND tipo='Empresa'",'','object');
	srand((float) microtime() * 10000000);
	$id_rand = array_rand($res);
	$nome = $res[$id_rand]->nome;
	$PA = number_format($res[$id_rand]->PA,2,',','.');
	$R = $res[$id_rand]->r;
	$id = $res[$id_rand]->id;
	$link = "$url?f=mostra_rede&id_empresa=$id&aba=grafico";
	$demo = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
	$demo .= "
<HTML>
<HEAD>
<META HTTP-EQUIV='Content-Type' CONTENT='text/html; charset=utf-8'>
<title>Ranking Proprietários do Brasil</title>
</HEAD>
<style type='text/css'>
  #header {
    background-image: url(http://www.maisdemocracia.org.br/uploads/wp-content/uploads/cabecalho.jpg);
    background-repeat: no-repeat no-repeat;
    background-position: 100% 100%;
  }
#banner {
width: 250px;
border: solid 1px #a80423;
background-color: #;
}
  
#banner-conteudo {
width: 250px;
margin: px;
}
#banner p, #banner a {
font-size: 9pt;
text-align: center;
font-family:\"Arial\";
margin-bottom:3px;
}
a, a:visited { color:#00f }
a:hover { text-decoration:none; color:#004; }

</style>
<BODY>
<div id='banner'>
    <img src='http://www.maisdemocracia.org.br/uploads/wp-content/uploads/cabecalho.jpg'>
    <div id='banner-conteudo'>
    	<center>
    	<p>Você conhece a <b>$nome</b>, cujo Poder Acumulado é de $PA milhões de reais? Não? <a href='$link' target='_blank'>Então, clique aqui</a> e conheça sua rede de poder e de outras empresas no Ranking dos Proprietários do Brasil!</p></center>
		<b><center> <br/> <a target='_blank' href='http://catarse.me/pt/portalproprietariosdobrasil'>Só com seu apoio conseguiremos revelar os verdadeiros proprietários do Brasil. Clique aqui e contribua com qualquer valor a partir de R$ 10,00</a></center></b></p></center>
	</div>
</div>
</BODY>
</HMTL>";
	if ($_REQUEST['demo'])
		echo $demo;
	else 
		echo json_encode(array(
			'nome'=>$nome, 
			'link'=>$link,
			'PA'=>$PA,
			'R'=>$R
			));
	exit;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<title>Ranking Proprietários do Brasil</title>
<link rel="stylesheet" type="text/css" href="../estilo.css" />
<script type="text/javascript">
	function mostraRede(id) {
		var janelaRedeVisual=window.open('mostra_rede.php?id='+id,'','height=600,width=800,toolbar=no,menubar=no,location=no,status=no,scrollbars=yes','mostrarede');
		if (window.focus) {janelaRedeVisual.focus()}
	}
</script>
<? if ($f=='mostra_rede')
	include('cabecalho_mostra_rede.php');
?>
</HEAD>	
<BODY>
<h1 class="stats"> Estatísticas de consulta </h1>
<h2 class="stats"> Consultas por dia </h1>
<div class="stats">
<table class="stats">
<thead><tr>
	<th>Dia</th>
	<th>Consultas</th>
</tr></thead>
<tbody>
<?
$sql = "SELECT count(*) as qtde, CONCAT(DAY(quando),'/',MONTH(quando),'/',YEAR(quando)) as dia FROM `logs` GROUP BY DATE(quando) ORDER BY quando DESC";
$res = faz_query($sql,'','object');
$total=0;
foreach ($res as $r) {
	$total += $r->qtde;
	?>
	<tr>
		<td><?=$r->dia?></td>
		<td><?=$r->qtde?></td>
	</tr>
	<?
}
?>
<tr class="total">
	<td><b>TOTAL</b></td>
	<td><b><?=$total?></b></td>
</tr>
</tbody></table>
</div>
<h2 class="stats"> Consultas por empresa </h1>
<div class="stats">
<table class="stats">
<thead><tr>
	<th>id</th>
	<th>Empresa ou pessoa</th>
	<th>Ranking (se listado)</th>
	<th>Consultas</th>
</tr></thead>
<tbody>
<?
$sql = "SELECT count(*) as qtde, b.nome, b.PA, b.IPA, a.id, b.r FROM `logs` a, assoc b WHERE a.id=b.id GROUP BY a.id ORDER BY qtde DESC";
$res = faz_query($sql,'','object');
foreach ($res as $r) {
	$ranking = ($r->r)
		? $r->r
		: "-";
	?>
	<tr>
		<td><?=$r->id?></td>
		<td><?=$r->nome?></td>
		<td><center><?=$ranking?></center></td>
		<td><?=$r->qtde?></td>
	</tr>
	<?
}
?>
<tr class="total">
	<td COLSPAN="3"><b>TOTAL</b></td>
	<td><b><?=$total?></b></td>
</tr>
</tbody></table>
</div>
<br><br>
</BODY>
</HTML>
